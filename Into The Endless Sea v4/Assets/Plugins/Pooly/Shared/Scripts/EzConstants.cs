// Copyright (c) 2016 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

namespace Ez
{
    public static partial class EzConstants
    {
        public const string SYMBOL_EZ_ADS = "EZ_ADS";
        public const string SYMBOL_EZ_ADS_UNITYADS = "EZ_ADS_UNITYADS";
        public const string SYMBOL_EZ_ADS_HEYZAP = "EZ_ADS_HEYZAP";
        public const string SYMBOL_EZ_ADS_AMAZON_MOBILEADS = "EZ_ADS_AMAZON_MOBILEADS";

        public const string SYMBOL_EZ_ANALYTICS = "EZ_ANALYTICS";
        public const string SYMBOL_EZ_ANALYTICS_UNITY = "EZ_ANALYTICS_UNITY";
        public const string SYMBOL_EZ_ANALYTICS_GAME_ANALYTICS = "EZ_ANALYTICS_GAME_ANALYTICS";
        public const string SYMBOL_EZ_ANALYTICS_GOOGLE_ANALYTICS = "EZ_ANALYTICS_GOOGLE";

        public const string SYMBOL_EZ_CORE = "EZ_CORE";

        public const string SYMBOL_EZ_DEFINE_SYMBOLS = "EZ_DEFINE_SYMBOLS";

        public const string SYMBOL_EZ_POOLY = "EZ_POOLY";
    }
}
