// Copyright (c) 2016 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using UnityEditor;
using UnityEngine;

namespace Ez.Pooly
{
    [InitializeOnLoad]
    public class PoolyLoader
    {
static string SpawnerIconImagesPath { get { return EFileHelper.GetRelativeDirectoryPath("Ez") + "/Pooly/Images/poolySpawnerIcon.png"; } }
        static string SpawnerIconGizmosPath { get { return "Assets/Gizmos/Pooly/poolySpawnerIcon.png"; } }
        static string SpawnerLocationIconImagesPath { get { return EFileHelper.GetRelativeDirectoryPath("Ez") + "/Pooly/Images/poolySpawnerLocationIcon.png"; } }
        static string SpawnerLocationIconGizmosPath { get { return "Assets/Gizmos/Pooly/poolySpawnerLocationIcon.png"; } }

        static PoolyLoader()
        {
            EzEditorUtility.AddScriptingDefineSymbol(EzConstants.SYMBOL_EZ_POOLY, EzEditorUtility.GetActiveBuildTargetGroup());
            EFileHelper.CreateDirectory("Assets/Gizmos/Pooly/");
            if ((Texture)AssetDatabase.LoadAssetAtPath(SpawnerIconGizmosPath, typeof(Texture)) == null) { AssetDatabase.CopyAsset(SpawnerIconImagesPath, SpawnerIconGizmosPath); }
            if ((Texture)AssetDatabase.LoadAssetAtPath(SpawnerLocationIconGizmosPath, typeof(Texture)) == null) { AssetDatabase.CopyAsset(SpawnerLocationIconImagesPath, SpawnerLocationIconGizmosPath); }
        }
    }
}
