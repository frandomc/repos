// Copyright (c) 2016 - 201 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace Ez.Pooly
{
    [CustomEditor(typeof(PoolyDespawner))]
    public class PoolyDespawnerEditor : EBaseEditor
    {
        private PoolyDespawner poolyDespawner { get { return (PoolyDespawner)target; } }

        SerializedProperty
            OnDespawn,
            despawnAfter,
            autoStart,
            duration,
            //randomDuration, randomDuraionMinimum, randomDurationMaximum,
            useParticleSystemDuration,
            useParticleSystemStartDelay,
            useParticleSystemStartLifetime,
            extraTime,
            playOnSpawn,
            orDespawnAfterTime,
            onlyWithTag, targetTag,
            despawnOnCollisionEnter, despawnOnCollisionStay, despawnOnCollisionExit,
            despawnOnTriggerEnter, despawnOnTriggerStay, despawnOnTriggerExit,
            despawnOnCollisionEnter2D, despawnOnCollisionStay2D, despawnOnCollisionExit2D,
            despawnOnTriggerEnter2D, despawnOnTriggerStay2D, despawnOnTriggerExit2D;

        void SerializedObjectFindProperties()
        {
            OnDespawn = serializedObject.FindProperty("OnDespawn");
            despawnAfter = serializedObject.FindProperty("despawnAfter");
            autoStart = serializedObject.FindProperty("autoStart");
            duration = serializedObject.FindProperty("duration");
            //randomDuration = serializedObject.FindProperty("randomDuration");
            //randomDuraionMinimum = serializedObject.FindProperty("randomDuraionMinimum");
            //randomDurationMaximum = serializedObject.FindProperty("randomDurationMaximum");
            useParticleSystemDuration = serializedObject.FindProperty("useParticleSystemDuration");
            useParticleSystemStartDelay = serializedObject.FindProperty("useParticleSystemStartDelay");
            useParticleSystemStartLifetime = serializedObject.FindProperty("useParticleSystemStartLifetime");
            extraTime = serializedObject.FindProperty("extraTime");
            playOnSpawn = serializedObject.FindProperty("playOnSpawn");
            orDespawnAfterTime = serializedObject.FindProperty("orDespawnAfterTime");
            onlyWithTag = serializedObject.FindProperty("onlyWithTag");
            targetTag = serializedObject.FindProperty("targetTag");
            despawnOnCollisionEnter = serializedObject.FindProperty("despawnOnCollisionEnter");
            despawnOnCollisionStay = serializedObject.FindProperty("despawnOnCollisionStay");
            despawnOnCollisionExit = serializedObject.FindProperty("despawnOnCollisionExit");
            despawnOnTriggerEnter = serializedObject.FindProperty("despawnOnTriggerEnter");
            despawnOnTriggerStay = serializedObject.FindProperty("despawnOnTriggerStay");
            despawnOnTriggerExit = serializedObject.FindProperty("despawnOnTriggerExit");
            despawnOnCollisionEnter2D = serializedObject.FindProperty("despawnOnCollisionEnter2D");
            despawnOnCollisionStay2D = serializedObject.FindProperty("despawnOnCollisionStay2D");
            despawnOnCollisionExit2D = serializedObject.FindProperty("despawnOnCollisionExit2D");
            despawnOnTriggerEnter2D = serializedObject.FindProperty("despawnOnTriggerEnter2D");
            despawnOnTriggerStay2D = serializedObject.FindProperty("despawnOnTriggerStay2D");
            despawnOnTriggerExit2D = serializedObject.FindProperty("despawnOnTriggerExit2D");
        }

        void GenerateInfoMessages()
        {
            warning = new System.Collections.Generic.Dictionary<string, EGUI.InfoMessage>();
            warning.Add("duration.floatValue", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "DESPAWNER IS DISABLED", message = "Having the duration set to zero (0) will instantly despawn this gameObject making this script redundant. Increase the duration to fix this issue." });
            warning.Add("poolyDespawner.aSource", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "DESPAWNER IS DISABLED", message = "No AudioSource was found on this gameObject or its children" });
            warning.Add("poolyDespawner.aSource.clip", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "DESPAWNER IS DISABLED", message = "The AudioSource does not have and AudioClip referenced" });
            warning.Add("poolyDespawner.pSystem", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "DESPAWNER IS DISABLED", message = "No ParticleSystem was found on this gameObject or its children" });
        }

        void OnEnable()
        {
            SerializedObjectFindProperties();
            GenerateInfoMessages();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            switch (poolyDespawner.despawnAfter)
            {
                case PoolyDespawner.DespawnAfter.Time: DrawHeader(EResources.HeaderBarPoolyDespawnerTime); DrawTime(); break;
                case PoolyDespawner.DespawnAfter.SoundPlayed: DrawHeader(EResources.HeaderBarPoolyDespawnerSound); DrawSound(); break;
                case PoolyDespawner.DespawnAfter.EffectPlayed: DrawHeader(EResources.HeaderBarPoolyDespawnerEffect); DrawEffect(); break;
                case PoolyDespawner.DespawnAfter.Collision: DrawHeader(EResources.HeaderBarPoolyDespawnerCollision); DrawCollision(); break;
                case PoolyDespawner.DespawnAfter.Trigger: DrawHeader(EResources.HeaderBarPoolyDespawnerTrigger); DrawTrigger(); break;
                case PoolyDespawner.DespawnAfter.Collision2D: DrawHeader(EResources.HeaderBarPoolyDespawnerCollision2D); DrawCollision2D(); break;
                case PoolyDespawner.DespawnAfter.Trigger2D: DrawHeader(EResources.HeaderBarPoolyDespawnerTrigger2D); DrawTrigger2D(); break;
            }
            EGUI.Space(SPACE_4);
            EGUI.PropertyField(OnDespawn, "OnDespawn", WIDTH_420);
            EGUI.Space(SPACE_4);
            serializedObject.ApplyModifiedProperties();
        }

        void DrawTime()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 70);
                EGUI.Space(8);
                EGUI.Label("Auto Start", 63);
                EGUI.PropertyField(autoStart, 70);
                EGUI.Space(8);
                EGUI.Label("Duration", 53);
                EGUI.PropertyField(duration, 40);
            }
            EGUI.EndHorizontal();
            if (duration.floatValue <= 0) { duration.floatValue = 0; }
            warning["duration.floatValue"].show.target = duration.floatValue <= 0;
            EGUI.DrawInfoMessage(warning["duration.floatValue"], WIDTH_420, EGUI.InfoMessageType.Warning);
        }

        void DrawSound()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 90);
                EGUI.Space(8);
                EGUI.PropertyField(playOnSpawn, 12);
                EGUI.Label("Play On Spawn", 90);
            }
            EGUI.EndHorizontal();
            if (poolyDespawner.aSource == null)
            {
                EGUI.Label("AudioSource: Not Found", WIDTH_420);
                warning["poolyDespawner.aSource"].show.target = true;
                warning["poolyDespawner.aSource.clip"].show.target = false;
            }
            else if (poolyDespawner.aSource.clip == null)
            {
                EGUI.Label("AudioSource: " + poolyDespawner.aSource.gameObject.name, WIDTH_420);
                EGUI.Label("AudioClip: Not Found", WIDTH_420);
                warning["poolyDespawner.aSource"].show.target = false;
                warning["poolyDespawner.aSource.clip"].show.target = true;
            }
            else
            {
                EGUI.Label("AudioSource: " + poolyDespawner.aSource.gameObject.name, WIDTH_420);
                EGUI.Label("AudioClip: " + poolyDespawner.aSource.clip.name, WIDTH_420);
                EGUI.Label("Duration: " + poolyDespawner.aSource.clip.length + " seconds", WIDTH_420);
                warning["poolyDespawner.aSource"].show.target = false;
                warning["poolyDespawner.aSource.clip"].show.target = false;
            }
            EGUI.DrawInfoMessage(warning["poolyDespawner.aSource"], WIDTH_420, EGUI.InfoMessageType.Warning);
            EGUI.DrawInfoMessage(warning["poolyDespawner.aSource.clip"], WIDTH_420, EGUI.InfoMessageType.Warning);
        }

        void DrawEffect()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 90);
                EGUI.Space(8);
                EGUI.PropertyField(playOnSpawn, 12);
                EGUI.Label("Play On Spawn", 90);
                EGUI.Space(8);
            }
            EGUI.EndHorizontal();
            if (poolyDespawner.pSystem == null)
            {
                EGUI.Label("ParticleSystem: Not Found", WIDTH_420);
                warning["poolyDespawner.pSystem"].show.target = true;
            }
            else
            {
                EGUI.Label("ParticleSystem: " + poolyDespawner.pSystem.gameObject.name, WIDTH_420);
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.PropertyField(useParticleSystemDuration, 12);
                    EGUI.Label("Duration: " + poolyDespawner.pSystem.duration + " seconds", WIDTH_420 - 12);
                }
                EGUI.EndHorizontal();
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.PropertyField(useParticleSystemStartDelay, 12);
                    EGUI.Label("Start Delay: " + poolyDespawner.pSystem.startDelay + " seconds", WIDTH_420);
                }
                EGUI.EndHorizontal();
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.PropertyField(useParticleSystemStartLifetime, 12);
                    EGUI.Label("Start Lifetime: " + poolyDespawner.pSystem.startLifetime + " seconds", WIDTH_420);
                }
                EGUI.EndHorizontal();
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.Label("Extra Time", 65);
                    EGUI.PropertyField(extraTime, 40);
                    EGUI.Label("seconds", 50);
                }
                EGUI.EndHorizontal();
                EGUI.Label("Total Duration: " + poolyDespawner.pSystemTotalDuration + " seconds", WIDTH_420);
                warning["poolyDespawner.pSystem"].show.target = false;
            }
            EGUI.DrawInfoMessage(warning["poolyDespawner.pSystem"], WIDTH_420, EGUI.InfoMessageType.Warning);
        }

        void DrawCollision()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 70);
                EGUI.Space(8);
                if (!orDespawnAfterTime.boolValue)
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After Time", 138);
                }
                else
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After", 106);
                    EGUI.PropertyField(duration, 40);
                    EGUI.Label("seconds", 53);
                }
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn OnCollision", 124);
                EGUI.Space(SPACE_4);
                EGUI.PropertyField(despawnOnCollisionEnter, 12);
                EGUI.Label("Enter", 34);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnCollisionStay, 12);
                EGUI.Label("Stay", 30);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnCollisionExit, 12);
                EGUI.Label("Exit", 26);
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.PropertyField(onlyWithTag, 12);
                EGUI.Label("Collide Only With Tag", 136);
                targetTag.stringValue = EditorGUILayout.TagField(targetTag.stringValue, GUILayout.Width(136));
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
        }

        void DrawTrigger()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 70);
                EGUI.Space(8);
                if (!orDespawnAfterTime.boolValue)
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After Time", 138);
                }
                else
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After", 106);
                    EGUI.PropertyField(duration, 40);
                    EGUI.Label("seconds", 53);
                }
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn OnTrigger", 124);
                EGUI.Space(SPACE_4);
                EGUI.PropertyField(despawnOnTriggerEnter, 12);
                EGUI.Label("Enter", 34);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnTriggerStay, 12);
                EGUI.Label("Stay", 30);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnTriggerExit, 12);
                EGUI.Label("Exit", 26);
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.PropertyField(onlyWithTag, 12);
                EGUI.Label("Trigger Only By Tag", 126);
                targetTag.stringValue = EditorGUILayout.TagField(targetTag.stringValue, GUILayout.Width(136));
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
        }

        void DrawCollision2D()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 80);
                EGUI.Space(8);
                if (!orDespawnAfterTime.boolValue)
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After Time", 138);
                }
                else
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After", 106);
                    EGUI.PropertyField(duration, 40);
                    EGUI.Label("seconds", 53);
                }
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn OnCollision", 124);
                EGUI.Space(SPACE_4);
                EGUI.PropertyField(despawnOnCollisionEnter2D, 12);
                EGUI.Label("Enter2D", 50);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnCollisionStay2D, 12);
                EGUI.Label("Stay2D", 46);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnCollisionExit2D, 12);
                EGUI.Label("Exit2D", 40);
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.PropertyField(onlyWithTag, 12);
                EGUI.Label("Collide Only With Tag", 132);
                targetTag.stringValue = EditorGUILayout.TagField(targetTag.stringValue, GUILayout.Width(136));
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
        }

        void DrawTrigger2D()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn After", 87);
                EGUI.PropertyField(despawnAfter, 80);
                EGUI.Space(8);
                if (!orDespawnAfterTime.boolValue)
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After Time", 138);
                }
                else
                {
                    EGUI.PropertyField(orDespawnAfterTime, 12);
                    EGUI.Label("Or Despawn After", 106);
                    EGUI.PropertyField(duration, 40);
                    EGUI.Label("seconds", 53);
                }
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Despawn OnTrigger", 124);
                EGUI.Space(SPACE_4);
                EGUI.PropertyField(despawnOnTriggerEnter2D, 12);
                EGUI.Label("Enter2D", 50);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnTriggerStay2D, 12);
                EGUI.Label("Stay2D", 46);
                EGUI.Space(SPACE_8);
                EGUI.PropertyField(despawnOnTriggerExit2D, 12);
                EGUI.Label("Exit2D", 40);
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.PropertyField(onlyWithTag, 12);
                EGUI.Label("Trigger Only By Tag", 126);
                targetTag.stringValue = EditorGUILayout.TagField(targetTag.stringValue, GUILayout.Width(136));
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
        }
    }
}
