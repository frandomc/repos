// Copyright (c) 2016 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.AnimatedValues;
using UnityEditorInternal;
using UnityEngine;

namespace Ez.Pooly
{
    [CustomEditor(typeof(PoolyExtension))]
    public class PoolyExtensionEditor : EzEditor
    {
        private PoolyExtension poolyExtension { get { return (PoolyExtension)target; } }

        /// <summary>
        /// ReorderableList that helps reorder the categories list
        /// </summary>
        ReorderableList rCategories;

        /// <summary>
        /// All the items sorted by category
        /// </summary>
        Dictionary<string, List<Pooly.Item>> categoryItem = new Dictionary<string, List<Pooly.Item>>();
        /// <summary>
        /// Each category expanded state
        /// </summary>
        Dictionary<string, AnimBool> categoryExpanded = new Dictionary<string, AnimBool>();
        /// <summary>
        /// Each item expanded state
        /// </summary>
        Dictionary<Pooly.Item, AnimBool> itemExpanded = new Dictionary<Pooly.Item, AnimBool>();

        /// <summary>
        /// Saves all the categories expanded state before we update the items list
        /// </summary>
        Dictionary<string, AnimBool> categoryExpandedBeforeUpdate = new Dictionary<string, AnimBool>();
        /// <summary>
        /// Saves all the items expanded state before we update the items list
        /// </summary>
        Dictionary<Pooly.Item, AnimBool> itemExpandedBeforeUpdate = new Dictionary<Pooly.Item, AnimBool>();

        /// <summary>
        /// Local Style
        /// </summary>
        GUIStyle barOpenCategory, barClosedCategory, barOpenGrey, barClosedGrey, barOpenRed, barClosedRed, itemBarInfoTextStyle, pooledItemSpawnedClonesTextStyle, settingsZoneInfoBoxStyle, dropZoneBoxStyle;

        string renameCategory, newCategoryName, deleteCategory, newItemCategory = string.Empty;

        /// <summary>
        /// List of categories used by the drop down list (contains 'New Category' and 'No Category' entries, by default)
        /// </summary>
        List<string> categoriesList = new List<string> { PoolyEditor.NEW_CATEGORY, Pooly.DEFAULT_CATEGORY_NAME };
        /// <summary>
        /// Index for the selected category in the drop down list from the categoriesList
        /// </summary>
        int newItemCategoryListIndex = 1;
        /// <summary>
        /// List of categories used by the reordable list
        /// </summary>
        List<string> categories = new List<string>();

        /// <summary>
        /// Toggle for whoing the 'Settings for New Items'
        /// </summary>
        AnimBool showNewItemSettings;
        /// <summary>
        /// Name for a new category created by the New Item menu (at the top)
        /// </summary>
        string newItemCategoryName = string.Empty;
        /// <summary>
        /// Deafult settings when creating/adding a new item
        /// </summary>
        Pooly.Item newItemSettings = new Pooly.Item();

        /// <summary>
        /// The item for which we are changing the category
        /// </summary>
        Pooly.Item changeCategoryItem = null;
        /// <summary>
        /// The category index for the change drop down list
        /// </summary>
        int changeCategoryItemIndex = 1;
        /// <summary>
        /// Name for a new category created by the Change Category menu (found inside every expanded Item)
        /// </summary>
        string changeCategoryNewCategory = string.Empty;

        /// <summary>
        /// The search pattern used by Regex to filter the Items
        /// </summary>
        string searchPattern = string.Empty;
        /// <summary>
        /// Variable used to determine if the searchPattern used generated any search results
        /// </summary>
        bool searchReturnedResuls = false;
        /// <summary>
        /// Contains all the Items that contain the searchPattern
        /// </summary>
        List<Pooly.Item> searchResults = new List<Pooly.Item>();

        /// <summary>
        /// Keeps track of all the Items with null prefabs (missing prefabs) and it is used to know when the developer drags and drops a new reference. Using this, we are able to check if the new reference is valid or not.
        /// </summary>
        List<Pooly.Item> itemsWithMissingPrefab = new List<Pooly.Item>();

        void SetupCustomStyles()
        {
            barOpenCategory = new GUIStyle
            {
                normal = { background = (Texture2D)EzResources.BtnBarBlueNormalOpen, textColor = EzColors.L_BLUE },
                active = { background = (Texture2D)EzResources.BtnBarBlueActive, textColor = EzColors.BLUE },
                fontSize = 13,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 30,
                padding = new RectOffset(24, 2, 1, 2),
                border = new RectOffset(1, 1, 1, 1),
                margin = new RectOffset(1, 1, 1, 1)
            };
            barClosedCategory = new GUIStyle
            {
                normal = { background = (Texture2D)EzResources.BtnBarBlueNormalClosed, textColor = EzColors.L_BLUE },
                active = { background = (Texture2D)EzResources.BtnBarBlueActive, textColor = EzColors.BLUE },
                fontSize = 13,
                fontStyle = FontStyle.Bold,
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 30,
                padding = new RectOffset(24, 2, 1, 2),
                border = new RectOffset(1, 1, 1, 1),
                margin = new RectOffset(1, 1, 1, 1)
            };
            barOpenGrey = new GUIStyle
            {
                normal = { background = (Texture2D)EzResources.BtnBarGreyNormalOpen, textColor = EzColors.UNITY_LIGHT },
                active = { background = (Texture2D)EzResources.BtnBarGreyActive, textColor = EzColors.UNITY_MILD },
                fontSize = 11,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 26,
                padding = new RectOffset(20, 2, 1, 2),
                border = new RectOffset(1, 1, 1, 1),
                margin = new RectOffset(1, 1, 1, 1)
            };
            barClosedGrey = new GUIStyle
            {
                normal = { background = (Texture2D)EzResources.BtnBarGreyNormalClosed, textColor = EzColors.UNITY_LIGHT },
                active = { background = (Texture2D)EzResources.BtnBarGreyActive, textColor = EzColors.UNITY_MILD },
                fontSize = 11,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 26,
                padding = new RectOffset(20, 2, 1, 2),
                border = new RectOffset(1, 1, 1, 1),
                margin = new RectOffset(1, 1, 1, 1)
            };
            barOpenRed = new GUIStyle
            {
                normal = { background = (Texture2D)EzResources.BtnBarRedNormalOpen, textColor = EzColors.L_RED },
                active = { background = (Texture2D)EzResources.BtnBarRedActive, textColor = EzColors.RED },
                fontSize = 11,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 26,
                padding = new RectOffset(20, 2, 1, 2),
                border = new RectOffset(1, 1, 1, 1),
                margin = new RectOffset(1, 1, 1, 1)
            };
            barClosedRed = new GUIStyle
            {
                normal = { background = (Texture2D)EzResources.BtnBarRedNormalClosed, textColor = EzColors.L_RED },
                active = { background = (Texture2D)EzResources.BtnBarRedActive, textColor = EzColors.RED },
                fontSize = 11,
                fontStyle = FontStyle.Normal,
                alignment = TextAnchor.MiddleLeft,
                fixedHeight = 26,
                padding = new RectOffset(20, 2, 1, 2),
                border = new RectOffset(1, 1, 1, 1),
                margin = new RectOffset(1, 1, 1, 1)
            };
            itemBarInfoTextStyle = new GUIStyle
            {
                normal = { textColor = EzColors.L_GREEN },
                fontSize = 11,
                fontStyle = FontStyle.Normal
            };
            pooledItemSpawnedClonesTextStyle = new GUIStyle
            {
                normal = { textColor = EditorGUIUtility.isProSkin ? EzColors.UNITY_LIGHT : EzColors.UNITY_DARK },
                fontSize = 9,
                fontStyle = FontStyle.Normal
            };
            settingsZoneInfoBoxStyle = new GUIStyle
            {
                normal =
                {
                    background = EzResources.HelpBoxDark,
                    textColor = EzColors.L_GREEN
                },
                fontSize = 10,
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.BoldAndItalic,
                border = new RectOffset(2, 2, 2, 2)
            };
            dropZoneBoxStyle = new GUIStyle
            {
                normal =
                {
                    background = EzResources.HelpBoxDark,
                    textColor = EzColors.L_BLUE
                },
                fontSize = 16,
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.BoldAndItalic,
                border = new RectOffset(2, 2, 2, 2)
            };
        }

        void UpdateCategoriesAndItems()
        {
            itemsWithMissingPrefab = new List<Pooly.Item>();
            categoryItem = new Dictionary<string, List<Pooly.Item>>();
            categoryExpanded = new Dictionary<string, AnimBool>();
            itemExpanded = new Dictionary<Pooly.Item, AnimBool>();
            categoriesList = new List<string> { PoolyEditor.NEW_CATEGORY, Pooly.DEFAULT_CATEGORY_NAME };
            categories = new List<string>();
            for (int i = 0; i < poolyExtension.items.Count; i++)
            {
                if (poolyExtension.items[i].prefab == null) { itemsWithMissingPrefab.Add(poolyExtension.items[i]); }
                poolyExtension.items[i].category = string.IsNullOrEmpty(poolyExtension.items[i].category) ? Pooly.DEFAULT_CATEGORY_NAME : poolyExtension.items[i].category;
                if (!categoryItem.ContainsKey(poolyExtension.items[i].category))
                {
                    categoryItem.Add(poolyExtension.items[i].category, new List<Pooly.Item>() { poolyExtension.items[i] });
                    if (!categoriesList.Contains(poolyExtension.items[i].category)) { categoriesList.Add(poolyExtension.items[i].category); }
                    if (!categories.Contains(poolyExtension.items[i].category)) { categories.Add(poolyExtension.items[i].category); }
                }
                else
                {
                    categoryItem[poolyExtension.items[i].category].Add(poolyExtension.items[i]);
                }

                if (!categoryExpanded.ContainsKey(poolyExtension.items[i].category))
                {
                    categoryExpanded.Add(poolyExtension.items[i].category, new AnimBool(false, Repaint));
                }
                itemExpanded.Add(poolyExtension.items[i], new AnimBool(false, Repaint));
            }

            if (!string.IsNullOrEmpty(renameCategory) && !string.IsNullOrEmpty(newCategoryName)) //if we just renamed a category we leave that category open
            {
                categoryExpanded[newCategoryName].value = true;
                renameCategory = string.Empty;
                newCategoryName = string.Empty;
            }

            if (!string.IsNullOrEmpty(newItemCategory)) //if we just added a new item to a category, we leave the category open
            {
                categoryExpanded[newItemCategory].value = true;
                newItemCategory = string.Empty;
            }

            rCategories = new ReorderableList(categories, typeof(string), true, false, false, false);
            rCategories.onReorderCallback = (list) =>
            {
                ReorderCategories();
            };
            rCategories.showDefaultBackground = false;
            rCategories.drawElementBackgroundCallback = (rect, index, active, focused) => { };
            rCategories.elementHeightCallback = (index) =>
            {
                Repaint();
                return 40;
            };
            rCategories.drawElementCallback = (rect, index, active, focused) =>
            {
                if (index == categories.Count) return;
                SaveCurrentColorsAndResetColors();
                if (GUI.Button(new Rect(rect.x + 6, rect.y - 6, WIDTH_1 - 26, 30), categories[index], barClosedCategory))
                {
                    categoryExpanded[categories[index]].target = true;
                }
                LoadPreviousColors();
            };

            if (itemExpandedBeforeUpdate.Count > 0) { itemExpanded = itemExpandedBeforeUpdate; itemExpandedBeforeUpdate = new Dictionary<Pooly.Item, AnimBool>(); }
            if (categoryExpandedBeforeUpdate.Count > 0) { categoryExpanded = categoryExpandedBeforeUpdate; categoryExpandedBeforeUpdate = new Dictionary<string, AnimBool>(); }
        }

        void ReorderCategories()
        {
            if (categories == null || categories.Count == 0) { return; }
            List<Pooly.Item> newItemsList = new List<Pooly.Item>();
            for (int i = 0; i < rCategories.list.Count; i++)
            {
                categoriesList.Add(rCategories.list[i].ToString());
                List<Pooly.Item> tempItemsList = new List<Pooly.Item>();
                for (int j = 0; j < poolyExtension.items.Count; j++)
                {
                    if (poolyExtension.items[j].category.Equals(rCategories.list[i].ToString())) { tempItemsList.Add(poolyExtension.items[j]); }
                }
                newItemsList.AddRange(tempItemsList);
                tempItemsList = new List<Pooly.Item>();
            }
            poolyExtension.items = new List<Pooly.Item>();
            poolyExtension.items = newItemsList;
            SortItemsListByCategory();
        }

        void SortItemsListByCategory()
        {
            if (poolyExtension.items == null || poolyExtension.items.Count == 0) { return; }
            List<Pooly.Item> newItemsList = new List<Pooly.Item>();
            for (int i = 0; i < categories.Count; i++)
            {
                List<Pooly.Item> tempItemsListWithPrefabs = new List<Pooly.Item>();
                List<Pooly.Item> tempItemsListWithNoPrefabs = new List<Pooly.Item>();
                for (int j = 0; j < poolyExtension.items.Count; j++)
                {
                    if (poolyExtension.items[j].category.Equals(categories[i]))
                    {
                        if (poolyExtension.items[j].prefab != null)
                        {
                            tempItemsListWithPrefabs.Add(poolyExtension.items[j]);
                        }
                        else
                        {
                            tempItemsListWithNoPrefabs.Add(poolyExtension.items[j]);
                        }
                    }
                }
                if (tempItemsListWithPrefabs.Count > 0)
                {
                    tempItemsListWithPrefabs.Sort((a, b) => a.prefab.name.CompareTo(b.prefab.name));
                    newItemsList.AddRange(tempItemsListWithPrefabs);
                }
                if (tempItemsListWithNoPrefabs.Count > 0)
                {
                    newItemsList.AddRange(tempItemsListWithNoPrefabs);
                }
            }
            poolyExtension.items = new List<Pooly.Item>();
            poolyExtension.items = newItemsList;
            categoryExpandedBeforeUpdate = categoryExpanded;
            itemExpandedBeforeUpdate = itemExpanded;
            UpdateCategoriesAndItems();
            GUIUtility.ExitGUI();
        }

        void CheckForReset()
        {
            if (poolyExtension.items.Count == 0 && categoryItem.Count > 0) //User pressed the Reset() button
            {
                UpdateCategoriesAndItems();
                newItemSettings = new Pooly.Item();
            }
        }

        void OnEnable()
        {
            showHelp = false;
            saying = EzResources.GetSaying;
            repaintOn = RepaintOn.Never;
            showNewItemSettings = new AnimBool(false, Repaint);
            SetupCustomStyles();
            UpdateCategoriesAndItems();
            MarkSceneDirty();
        }

        public override void OnInspectorGUI()
        {
            CheckForReset();
            SetBaseColor();
            serializedObject.Update();
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            DrawHeader(EResources.HeaderBarPoolyExtension);
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            DrawHeaderToolbar(null, null);
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            if (EditorApplication.isPlaying)
            {
                float fullWidth = WIDTH_1;
                float indentWidth = WIDTH_1 - INDENT;
                if (poolyExtension.Categories.Count > 0)
                {
                    SaveCurrentColorsAndResetColors();
                    foreach (var category in poolyExtension.Categories.Keys)
                    {
                        if (!categoryExpanded.ContainsKey(category))
                        {
                            categoryExpanded.Add(category, new AnimBool(true, Repaint));
                        }
                        if (GUILayout.Button(category,
                                    categoryExpanded[category].target ? barOpenCategory : barClosedCategory,
                                    GUILayout.Width(WIDTH_1)))
                        {
                            categoryExpanded[category].target = !categoryExpanded[category].target;
                            GUIUtility.keyboardControl = 0;
                        }
                        if (EditorGUILayout.BeginFadeGroup(categoryExpanded[category].faded))
                        {
                            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                            EditorGUILayout.BeginHorizontal(GUILayout.Width(fullWidth));
                            {
                                EzEditorUtility.HorizontalSpace((int)INDENT);
                                EditorGUILayout.BeginVertical(GUILayout.Width(indentWidth - 22));
                                {
                                    foreach (var pooledItem in poolyExtension.Pool.Values)
                                    {
                                        if (pooledItem.prefab != null && pooledItem.category.Equals(category))
                                        {
                                            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                                            EditorGUILayout.LabelField(pooledItem.prefab.name, GUILayout.Width(indentWidth - 22));
                                            GUILayout.Space(-20);
                                            DrawItemQuickViewInfo(pooledItem, indentWidth - 2);
                                            EditorGUILayout.LabelField(pooledItem.ActiveClones.Count + " Spawned Clones | " + pooledItem.DisabledClones.Count + " Available Clones",
                                                                       pooledItemSpawnedClonesTextStyle,
                                                                       GUILayout.Width(indentWidth - 22));
                                            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                                            EzEditorUtility.DrawTexture(EzResources.BtnGreyMildActive, indentWidth - 22, 1);
                                        }
                                    }
                                }
                                EditorGUILayout.EndVertical();
                            }
                            EditorGUILayout.EndHorizontal();
                            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
                        }
                        EditorGUILayout.EndFadeGroup();
                        EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
                    }
                    Repaint();
                }
            }
            else
            {
                PoolyEditor.DragAndDropCheck(Event.current.type);
                SaveCurrentColorsAndResetColors();
                DrawAddMissingItemsAndDropPrefab();
                DrawHelpBox(showHelp, PoolyEditor.HELP_DROP_PREFABS_HERE, "Drop Prefabs Here");
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                DrawNewItemArea();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                DrawSearchArea();
                if (!GenerateSearchResults())
                {
                    if (AreAllCategoriesClosed())
                    {
                        EditorGUILayout.BeginVertical();
                        {
                            GUILayout.Space(-9);
                            rCategories.DoLayoutList();
                        }
                        EditorGUILayout.EndVertical();
                    }
                    else
                    {
                        EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                        CheckItemsWithMissingPrefabs();
                        DrawItems();
                    }
                }
            }
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            EzEditorUtility.ResetColors();
            serializedObject.ApplyModifiedProperties();
        }

        void CheckItemsWithMissingPrefabs()
        {
            if (Event.current.type == EventType.Layout) return;
            if (itemsWithMissingPrefab.Count == 0) { return; }
            for (int i = itemsWithMissingPrefab.Count - 1; i >= 0; i--)
            {
                if (itemsWithMissingPrefab[i].prefab != null) //the dev added a prefab by drag and drop
                {
                    if (PoolyEditor.PrefabExistsInPool(itemsWithMissingPrefab[i].prefab, poolyExtension.items))
                    {
                        EditorUtility.DisplayDialog("Duplicate prefab found in the pool!", "The '" + itemsWithMissingPrefab[i].prefab.name + "' prefab already exists in the pool.\n\nYou cannot have different prefabs with the same name.", "Ok");
                        itemsWithMissingPrefab[i].prefab = null;
                        GUIUtility.ExitGUI();
                    }
                    else //no duplicate prefabs found --> we sort the pool and we remove this prefab from the missing list
                    {
                        itemsWithMissingPrefab.RemoveAt(i);
                        SortItemsListByCategory();
                    }
                }
            }
        }

        bool AreAllCategoriesClosed()
        {
            if (categoryExpanded != null && categoryExpanded.Count > 0)
            {
                foreach (var c in categoryExpanded.Keys)
                {
                    if (categoryExpanded[c].target) return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Draws the item's settings.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="width"></param>
        void DrawItemSettings(Pooly.Item item, float width)
        {
            if (!item.allowRecycleClones)
            {
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width));
                {
                    GUILayout.Space(6);
                    EditorGUILayout.BeginVertical(GUILayout.Width(14), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        GUILayout.FlexibleSpace();
                        EzEditorUtility.DrawTexture(item.allowInstantiateMore ? EResources.IconPoolyInstantiateMoreActive : EResources.IconPoolyInstantiateMore, 14, 14);
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(GUILayout.Width(300), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        item.allowInstantiateMore = EditorGUILayout.ToggleLeft("Instantiate more clones, if needed", item.allowInstantiateMore, GUILayout.Width(300));
                        GUILayout.FlexibleSpace();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (item.allowInstantiateMore) { item.allowInstantiateMore = false; }
            if (item.allowInstantiateMore)
            {
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.Space(23);
                    EditorGUILayout.BeginVertical(GUILayout.Width(22), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        GUILayout.FlexibleSpace();
                        EzEditorUtility.DrawTexture(item.limitCloneCount ? EResources.IconPoolyMaxActive : EResources.IconPoolyMax, 20, 20);
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(GUILayout.Width(400), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        EditorGUILayout.BeginHorizontal(GUILayout.Width(400), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                        {
                            item.limitCloneCount = EditorGUILayout.ToggleLeft("Limit the max number of clones" + (item.limitCloneCount ? " to" : ""), item.limitCloneCount, GUILayout.Width(212));
                            if (item.limitCloneCount)
                            {
                                item.cloneCountLimit = EditorGUILayout.IntField(item.cloneCountLimit, GUILayout.Width(62));
                                EditorGUILayout.LabelField(item.cloneCountLimit == 1 ? "clone" : "clones", GUILayout.Width(40));
                                if (item.cloneCountLimit < item.preloadCloneCount) { item.cloneCountLimit = item.preloadCloneCount; }
                            }
                            GUILayout.FlexibleSpace();
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS / 2);
            }
            if (!item.allowInstantiateMore && item.limitCloneCount) { item.limitCloneCount = false; }
            if (!item.allowInstantiateMore)
            {
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width));
                {
                    GUILayout.Space(6);
                    EditorGUILayout.BeginVertical(GUILayout.Width(14), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        GUILayout.FlexibleSpace();
                        EzEditorUtility.DrawTexture(item.allowRecycleClones ? EResources.IconPoolyRecycleActive : EResources.IconPoolyRecycle, 14, 14);
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(GUILayout.Width(400), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        item.allowRecycleClones = EditorGUILayout.ToggleLeft("Recycle the oldest spawned clone, if needed", item.allowRecycleClones, GUILayout.Width(400));
                        GUILayout.FlexibleSpace();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
            }
            else if (item.allowRecycleClones) { item.allowRecycleClones = false; }

            EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
            {
                GUILayout.Space(6);
                EditorGUILayout.BeginVertical(GUILayout.Width(14), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                {
                    GUILayout.FlexibleSpace();
                    EzEditorUtility.DrawTexture(item.limitCloneCreationPerFrame ? EResources.IconPoolyWaitActive : EResources.IconPoolyWait, 14, 14);
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(GUILayout.Width(400), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                {
                    item.limitCloneCreationPerFrame = EditorGUILayout.ToggleLeft("Limit the number of clones created each frame", item.limitCloneCreationPerFrame, GUILayout.Width(400));
                    GUILayout.FlexibleSpace();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
            if (item.limitCloneCreationPerFrame)
            {
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.Space(37);
                    EditorGUILayout.LabelField("Create", GUILayout.Width(42));
                    item.clonesOnFirstFrame = EditorGUILayout.IntField(item.clonesOnFirstFrame, GUILayout.Width(62));
                    if (item.clonesOnFirstFrame < 0) { item.clonesOnFirstFrame = 0; }
                    EditorGUILayout.LabelField((item.clonesOnFirstFrame == 1 ? "clone" : "clones") + " on the first frame", GUILayout.Width(144));
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.Space(37);
                    EditorGUILayout.LabelField("Then wait for", GUILayout.Width(78));
                    item.delayCreatingClonesForFrames = EditorGUILayout.IntField(item.delayCreatingClonesForFrames, GUILayout.Width(62));
                    if (item.delayCreatingClonesForFrames < 0) { item.delayCreatingClonesForFrames = 0; }
                    EditorGUILayout.LabelField((item.delayCreatingClonesForFrames == 1 ? "frame" : "frames"), GUILayout.Width(140));
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.Space(37);
                    EditorGUILayout.LabelField("Then create", GUILayout.Width(72));
                    item.clonesPerFrame = EditorGUILayout.IntField(item.clonesPerFrame, GUILayout.Width(62));
                    if (item.clonesPerFrame < 1) { item.clonesPerFrame = 1; }
                    EditorGUILayout.LabelField((item.clonesPerFrame == 1 ? "clone" : "clones") + " each frame", GUILayout.Width(140));
                }
                EditorGUILayout.EndHorizontal();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS / 2);
            }
            EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
            {
                GUILayout.Space(6);
                EditorGUILayout.BeginVertical(GUILayout.Width(14), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                {
                    GUILayout.FlexibleSpace();
                    EzEditorUtility.DrawTexture(item.debug ? EResources.IconPoolyDebugActive : EResources.IconPoolyDebug, 14, 14);
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(GUILayout.Width(190), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                {
                    item.debug = EditorGUILayout.ToggleLeft("Log debug messages", item.debug, GUILayout.Width(190));
                    GUILayout.FlexibleSpace();
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }

        void DrawNewItemArea()
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
            {
                SaveCurrentColorsAndResetColors();
                if (newItemCategoryListIndex >= categoriesList.Count) { newItemCategoryListIndex = 1; } //fix for a strange bug when the index gets out of bounds - hard reset to the 'No Category' selection
                if (GUILayout.Button("Create a New Item", skin.GetStyle(EzStyle.StyleName.BtnGreen.ToString()), GUILayout.Width(WIDTH_3)))
                {
                    if (categoriesList[newItemCategoryListIndex].Equals(PoolyEditor.NEW_CATEGORY))
                    {
                        if (string.IsNullOrEmpty(newItemCategoryName))
                        {
                            EditorUtility.DisplayDialog("Create New Item", "Please enter a category name to continue.", "Ok");
                        }
                        else if (newItemCategoryName.Trim().Equals(PoolyEditor.NEW_CATEGORY))
                        {
                            EditorUtility.DisplayDialog("Create New Item", "You cannot create a new category named '" + PoolyEditor.NEW_CATEGORY + "'.", "Ok");
                        }
                        else
                        {
                            AddItemToCategory(newItemCategoryName, null);
                            newItemCategoryListIndex = categoriesList.FindIndex(x => x.Equals(newItemCategoryName));
                            newItemCategoryName = string.Empty;
                        }
                    }
                    else
                    {
                        AddItemToCategory(categoriesList[newItemCategoryListIndex], null);
                    }
                    GUIUtility.keyboardControl = 0;
                }
                LoadPreviousColors();
                EditorGUILayout.LabelField("in Category", GUILayout.Width(70));
                if (categoriesList[newItemCategoryListIndex].Equals(PoolyEditor.NEW_CATEGORY))
                {
                    EzEditorUtility.VerticalSpace(-1);
                    newItemCategoryName = EditorGUILayout.TextField(newItemCategoryName, GUILayout.Width(WIDTH_1 - WIDTH_3 - 70 - 1 - 50 - 1 - 12));
                    SaveCurrentColorsAndResetColors();
                    GUILayout.Space(1);
                    if (GUILayout.Button("Cancel", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(50), GUILayout.Height(18)))
                    {
                        newItemCategoryListIndex = 1;
                        newItemCategoryName = string.Empty;
                        GUIUtility.keyboardControl = 0;
                    }
                    LoadPreviousColors();
                }
                else
                {
                    newItemCategoryListIndex = EditorGUILayout.Popup(newItemCategoryListIndex, categoriesList.ToArray(), GUILayout.Width(WIDTH_1 - WIDTH_3 - 70 - 8));
                }
            }
            EditorGUILayout.EndHorizontal();
            DrawHelpBox(showHelp, PoolyEditor.HELP_CREATE_NEW_ITEM, "Create a New Item");
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            SaveCurrentColorsAndResetColors();
            EditorGUILayout.LabelField("", settingsZoneInfoBoxStyle, GUILayout.Width(WIDTH_1), GUILayout.Height(20));
            GUILayout.Space(-21);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
            {
                GUILayout.Space(2);
                DrawItemQuickViewInfo(newItemSettings, WIDTH_1, true, false);
            }
            EditorGUILayout.EndHorizontal();

            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS / 2);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
            {
                if (GUILayout.Button("Settings for New Items", showNewItemSettings.target ? barOpenGrey : barClosedGrey, GUILayout.Width(WIDTH_1 - 120 - 4)))
                {
                    showNewItemSettings.target = !showNewItemSettings.target;
                    GUIUtility.keyboardControl = 0;
                }
                GUILayout.Space(1);
                EditorGUILayout.BeginVertical(GUILayout.Width(100), GUILayout.Height(20));
                {
                    GUILayout.Space(4);
                    if (GUILayout.Button("reset to default", skin.GetStyle(EzStyle.StyleName.BtnGreyMild.ToString()), GUILayout.Width(120), GUILayout.Height(22)))
                    {
                        newItemSettings = new Pooly.Item();
                        showNewItemSettings.target = false;
                        GUIUtility.keyboardControl = 0;
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
            DrawHelpBox(showHelp, PoolyEditor.HELP_SETTINGS_FOR_NEW_ITEMS);
            LoadPreviousColors();
            if (EditorGUILayout.BeginFadeGroup(showNewItemSettings.faded))
            {
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
                {
                    GUILayout.Space(6);
                    EditorGUILayout.BeginVertical(GUILayout.Width(14), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                    {
                        GUILayout.FlexibleSpace();
                        EzEditorUtility.DrawTexture(EResources.IconPoolyPreloadNumberActive, 14, 14);
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.LabelField("Number of Clones", GUILayout.Width(108));
                    newItemSettings.preloadCloneCount = EditorGUILayout.IntField(newItemSettings.preloadCloneCount, GUILayout.Width(86));
                }
                EditorGUILayout.EndHorizontal();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                DrawItemSettings(newItemSettings, WIDTH_1);
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            }
            EditorGUILayout.EndFadeGroup();
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
            EzEditorUtility.DrawTexture(EzResources.BtnGreyMildActive, WIDTH_1, 2);
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
        }

        void DrawSearchArea()
        {
            SaveCurrentColorsAndResetColors();
            EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
            {
                EditorGUILayout.LabelField("Search", GUILayout.Width(44));
                searchPattern = EditorGUILayout.TextField(searchPattern, GUILayout.Width(WIDTH_1 - 44 - 10 - 120));
                GUILayout.Space(1);
                if (GUILayout.Button(string.IsNullOrEmpty(searchPattern) ? "collapse all" : "clear search", skin.GetStyle(EzStyle.StyleName.BtnGreyMild.ToString()), GUILayout.Width(120)))
                {
                    foreach (var item in poolyExtension.items) //close all the items and all the categories
                    {
                        itemExpanded[item].target = false;
                        categoryExpanded[item.category].target = false;
                    }
                    foreach (var item in poolyExtension.items) //open only the ones from the search
                    {
                        if (searchResults.Contains(item))
                        {
                            itemExpanded[item].target = true;
                            categoryExpanded[item.category].target = true;
                        }
                    }
                    searchResults = new List<Pooly.Item>();
                    searchPattern = string.Empty;
                    renameCategory = string.Empty; //just in case we have a rename option active
                    newCategoryName = string.Empty; //just in case we have a new category option active
                    GUIUtility.keyboardControl = 0;
                }
            }
            EditorGUILayout.EndHorizontal();
            DrawHelpBox(showHelp, PoolyEditor.HELP_SEARCH, "Search");
            DrawHelpBox(showHelp,
                        string.IsNullOrEmpty(searchPattern) ? PoolyEditor.HELP_COLLAPSE_ALL : PoolyEditor.HELP_CLEAR_SEARCH,
                        string.IsNullOrEmpty(searchPattern) ? "collapse all" : "clear search");
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
            EzEditorUtility.DrawTexture(EzResources.BtnGreyMildActive, WIDTH_1, 2);
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            LoadPreviousColors();
        }

        bool GenerateSearchResults()
        {
            if (string.IsNullOrEmpty(searchPattern)) { return false; };
            searchReturnedResuls = false;
            searchResults = new List<Pooly.Item>();
            string input = string.Empty;
            for (int i = 0; i < poolyExtension.items.Count; i++)
            {
                input = poolyExtension.items[i].prefab != null ? poolyExtension.items[i].prefab.name : "Missing Prefab";
                try
                {
                    if (Regex.IsMatch(input, searchPattern, RegexOptions.IgnoreCase))
                    {
                        searchResults.Add(poolyExtension.items[i]);
                        DrawItem(poolyExtension.items[i], WIDTH_1);
                        EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                        searchReturnedResuls = true;
                    }
                }
                catch (System.Exception)
                {
                    Debug.Log("[Pooly] Invalid search pattern.");
                }
            }
            if (!searchReturnedResuls)
            {
                EditorGUILayout.LabelField("Your search returned no results.");
            }
            return true;
        }

        public void DrawAddMissingItemsAndDropPrefab()
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
            {
                EditorGUILayout.BeginVertical(GUILayout.Width(WIDTH_1));
                {
                    //GUILayout.Space(-22);
                    EditorGUILayout.LabelField("Drop Prefabs Here", dropZoneBoxStyle, GUILayout.Width(WIDTH_1 - 4), GUILayout.Height(40));
                    Rect dropZone = GUILayoutUtility.GetLastRect();
                    if (Event.current.type == EventType.DragPerform)
                    {
                        if (dropZone.Contains(Event.current.mousePosition))
                        {
                            Event.current.Use();
                            DragAndDrop.AcceptDrag();
                            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                            string targetCategory = Pooly.DEFAULT_CATEGORY_NAME;
                            if (categoriesList[newItemCategoryListIndex].Equals(PoolyEditor.NEW_CATEGORY))
                            {
                                if (!string.IsNullOrEmpty(newItemCategoryName))
                                {
                                    targetCategory = newItemCategoryName;
                                }

                            }
                            else
                            {
                                targetCategory = categoriesList[newItemCategoryListIndex];
                            }
                            foreach (var obj in DragAndDrop.objectReferences)
                            {
                                if (obj.GetType() != typeof(GameObject))
                                {
                                    EditorUtility.DisplayDialog("Cannot add New Item to the pool!", "The '" + obj.name + "' object you are trying to add to the pool is not a gameObject.", "Ok");
                                }
                                else if (PrefabUtility.GetPrefabObject(obj) == null) // If TRUE then this is not a prefab
                                {
                                    EditorUtility.DisplayDialog("Cannot add New Item to the pool!", "The '" + obj.name + "' gameObject you are trying to add to the pool is not a prefab.\nYou cannot and should not try to add scene objects.", "Ok");
                                }
                                else
                                {
                                    GameObject go = (GameObject)obj;
                                    AddItemToCategory(targetCategory, go.transform);
                                    newItemCategoryListIndex = categoriesList.FindIndex(x => x.Equals(targetCategory));
                                    newItemCategoryName = string.Empty;
                                }
                            }
                        }
                    }
                }
                EditorGUILayout.EndVertical();
            }
            EditorGUILayout.EndHorizontal();
        }

        void AddItemToCategory(string category, Transform prefab)
        {
            Pooly.Item newItem = new Pooly.Item
            {
                category = category,
                prefab = prefab,
                preloadCloneCount = newItemSettings.preloadCloneCount,
                limitCloneCount = newItemSettings.limitCloneCount,
                cloneCountLimit = newItemSettings.cloneCountLimit,
                limitCloneCreationPerFrame = newItemSettings.limitCloneCreationPerFrame,
                clonesOnFirstFrame = newItemSettings.clonesOnFirstFrame,
                clonesPerFrame = newItemSettings.clonesPerFrame,
                delayCreatingClonesForFrames = newItemSettings.delayCreatingClonesForFrames,
                allowInstantiateMore = newItemSettings.allowInstantiateMore,
                allowRecycleClones = newItemSettings.allowRecycleClones,
                debug = newItemSettings.debug
            };

            if (prefab != null && PoolyEditor.ItemExistInPool(newItem, poolyExtension.items))
            {
                EditorUtility.DisplayDialog("Cannot add New Item to the pool!", "The '" + newItem.prefab.name + "' prefab already exists in the pool.\n\nYou cannot have different prefabs with the same name.", "Ok");
                return;
            }

            poolyExtension.items.Add(newItem);
            PoolyEditor.CloseCategoryItems(category, itemExpanded);
            if (!categoryItem.ContainsKey(category)) { categoryItem.Add(category, new List<Pooly.Item>()); categoriesList.Add(category); categories.Add(category); }
            categoryItem[category].Add(newItem);
            if (!categoryExpanded.ContainsKey(category)) { categoryExpanded.Add(category, new AnimBool(false, Repaint)); }
            categoryExpanded[category].target = true;
            itemExpanded.Add(newItem, new AnimBool(true, Repaint));
            if (prefab == null)
            {
                itemsWithMissingPrefab.Add(newItem);
            }
        }

        void DeleteItem(Pooly.Item item)
        {
            if (categoryItem[item.category].Count == 1)
            {
                if (!EditorUtility.DisplayDialog("Delete Item", "This was the last item of the '" + item.category + "' category. The category will get deteled as well.", "Ok", "Cancel"))
                {
                    return;
                }
            }
            categoryItem[item.category].Remove(item);
            if (categoryItem[item.category].Count == 0)
            {
                categoryItem.Remove(item.category);
                categoryExpanded.Remove(item.category);
                categories.Remove(item.category);
                if (!item.category.Equals(Pooly.DEFAULT_CATEGORY_NAME))
                {
                    categoriesList.Remove(item.category);
                }
                newItemCategoryListIndex = 1;
            }
            itemExpanded.Remove(item);
            poolyExtension.items.Remove(item);
            EditorGUIUtility.ExitGUI();
        }

        void RenameCategory()
        {
            if (string.IsNullOrEmpty(renameCategory)) return;
            if (string.IsNullOrEmpty(newCategoryName.Trim())) { EditorUtility.DisplayDialog("Rename Category", "Cannot rename the category to an empty name.", "Ok"); return; }
            newCategoryName = newCategoryName.Trim();
            if (newCategoryName.Equals(PoolyEditor.NEW_CATEGORY)) { EditorUtility.DisplayDialog("Rename Category", "Cannot rename the category to '" + PoolyEditor.NEW_CATEGORY + "'.", "Ok"); return; }
            if (newCategoryName.Equals(Pooly.DEFAULT_CATEGORY_NAME)) { EditorUtility.DisplayDialog("Rename Category", "Cannot rename the category to '" + Pooly.DEFAULT_CATEGORY_NAME + "'.", "Ok"); return; }
            if (renameCategory.Equals(newCategoryName)) { renameCategory = string.Empty; newCategoryName = string.Empty; return; }
            if (categoriesList.Contains(newCategoryName)) //this is a merge
            {
                newItemCategoryListIndex = 1;
                if (!newCategoryName.Equals(Pooly.DEFAULT_CATEGORY_NAME))
                {
                    categoriesList.Remove(newCategoryName);
                    categories.Remove(newCategoryName);
                }
            }
            else
            {
                categoryExpanded.Add(newCategoryName, categoryExpanded[renameCategory]);
                categoryExpanded.Remove(renameCategory);
            }
            foreach (var item in poolyExtension.items)
            {
                if (item.category.Equals(renameCategory))
                {
                    item.category = newCategoryName;
                }
            }
            categoryExpandedBeforeUpdate = categoryExpanded;
            itemExpandedBeforeUpdate = itemExpanded;
            UpdateCategoriesAndItems();
            EditorGUIUtility.ExitGUI();
        }

        void DrawButtonAddItemToCategory(string category)
        {
            if (GUILayout.Button("Add Item", skin.GetStyle(EzStyle.StyleName.BtnGreen.ToString()), GUILayout.Width(WIDTH_3 - 4), GUILayout.Height(18 * categoryExpanded[category].faded)))
            {
                AddItemToCategory(category, null);
                GUIUtility.keyboardControl = 0;
            }
        }
        void DrawButtonRenameCategory(string category)
        {
            if (GUILayout.Button("Rename Category", skin.GetStyle(EzStyle.StyleName.BtnOrange.ToString()), GUILayout.Width(WIDTH_3 - 4), GUILayout.Height(18 * categoryExpanded[category].faded)))
            {
                renameCategory = category;
                newCategoryName = category;
                GUIUtility.keyboardControl = 0;
            }
        }
        void DrawRenameCategoryZone()
        {
            newCategoryName = EditorGUILayout.TextField(newCategoryName, GUILayout.Width(306));
            if (GUILayout.Button("Ok", skin.GetStyle(EzStyle.StyleName.BtnGreen.ToString()), GUILayout.Width(50), GUILayout.Height(18)))
            {
                RenameCategory();
                GUIUtility.keyboardControl = 0;
            }
            GUILayout.Space(1);
            if (GUILayout.Button("Cancel", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(50), GUILayout.Height(18)))
            {
                renameCategory = string.Empty;
                newCategoryName = string.Empty;
                GUIUtility.keyboardControl = 0;
            }
        }
        void DrawButtonDeleteCategory(string category)
        {
            if (GUILayout.Button("Delete Category", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(WIDTH_3 - 4), GUILayout.Height(18 * categoryExpanded[category].faded)))
            {
                if (EditorUtility.DisplayDialog("Delete Category", "Are you sure you want to delete the '" + category + "' Category? This will delete all the items inside of it as well. Operation cannot be undone!", "Ok", "Cancel"))
                {
                    GUIUtility.keyboardControl = 0;
                    for (int i = poolyExtension.items.Count - 1; i >= 0; i--) { if (poolyExtension.items[i].category.Equals(category)) { poolyExtension.items.RemoveAt(i); } }
                    if (!category.Equals(Pooly.DEFAULT_CATEGORY_NAME)) { categoriesList.Remove(category); }
                    categories.Remove(category);
                    newItemCategoryListIndex = categoriesList.FindIndex(x => x.Equals(Pooly.DEFAULT_CATEGORY_NAME));
                    categoryItem.Remove(category);
                    categoryExpanded.Remove(category);
                    categoryExpandedBeforeUpdate = categoryExpanded;
                    itemExpandedBeforeUpdate = itemExpanded;
                    UpdateCategoriesAndItems();
                    GUIUtility.ExitGUI();
                }
            }
        }

        void DrawCategoryOptionsButtons(string category)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(WIDTH_1));
            {
                GUILayout.Space(4);
                if (string.IsNullOrEmpty(renameCategory) || !category.Equals(renameCategory)) //if the renameCategory is empty or if this is not the category we are renaming, we show the category option buttons
                {
                    DrawButtonAddItemToCategory(category);
                    GUILayout.Space(1);
                    DrawButtonRenameCategory(category);
                    GUILayout.Space(1);
                    DrawButtonDeleteCategory(category);
                }
                else //this is the cateogry we are currently renaming so we draw the rename category zone
                {
                    DrawRenameCategoryZone();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        public void DrawItemQuickViewInfo(Pooly.Item item, float width, bool alignLeft = false, bool alignCenter = false)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
            {
                if (!alignLeft) { GUILayout.FlexibleSpace(); }
                EditorGUILayout.BeginVertical(GUILayout.Width(12), GUILayout.Height(20));
                {
                    GUILayout.Space(3);
                    EzEditorUtility.DrawTexture(EResources.IconPoolyPreloadNumberActive, 12, 12);
                    GUILayout.Space(5);
                }
                EditorGUILayout.EndVertical();
                EditorGUILayout.LabelField("[" + item.preloadCloneCount.ToString() + "]", itemBarInfoTextStyle, GUILayout.Width(8 + 6 * (item.preloadCloneCount.ToString().Length + 1)), GUILayout.Height(20));
                if (item.allowInstantiateMore)
                {
                    EditorGUILayout.BeginVertical(GUILayout.Width(12), GUILayout.Height(20));
                    {
                        GUILayout.Space(3);
                        EzEditorUtility.DrawTexture(EResources.IconPoolyInstantiateMoreActive, 12, 12);
                        GUILayout.Space(5);
                    }
                    EditorGUILayout.EndVertical();
                }
                if (item.limitCloneCount)
                {
                    GUILayout.Space(4);
                    EditorGUILayout.BeginVertical(GUILayout.Width(18), GUILayout.Height(20));
                    {
                        GUILayout.Space(0);
                        EzEditorUtility.DrawTexture(EResources.IconPoolyMaxActive, 18, 18);
                        GUILayout.Space(2);
                    }
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.LabelField("[" + item.cloneCountLimit.ToString() + "]", itemBarInfoTextStyle, GUILayout.Width(8 + 6 * (item.cloneCountLimit.ToString().Length + 1)), GUILayout.Height(20));
                }
                if (item.allowRecycleClones)
                {
                    GUILayout.Space(2);
                    EditorGUILayout.BeginVertical(GUILayout.Width(12), GUILayout.Height(20));
                    {
                        GUILayout.Space(3);
                        EzEditorUtility.DrawTexture(EResources.IconPoolyRecycleActive, 12, 12);
                        GUILayout.Space(5);
                    }
                    EditorGUILayout.EndVertical();
                }
                if (item.limitCloneCreationPerFrame)
                {
                    GUILayout.Space(4);
                    EditorGUILayout.BeginVertical(GUILayout.Width(12), GUILayout.Height(20));
                    {
                        GUILayout.Space(3);
                        EzEditorUtility.DrawTexture(EResources.IconPoolyWaitActive, 12, 12);
                        GUILayout.Space(5);
                    }
                    EditorGUILayout.EndVertical();
                    string clonesPerFrame = "(" + item.clonesOnFirstFrame + "/" + item.delayCreatingClonesForFrames + "/" + item.clonesPerFrame + ")";
                    EditorGUILayout.LabelField(clonesPerFrame, itemBarInfoTextStyle, GUILayout.Width(6 * (clonesPerFrame.Length + 1)), GUILayout.Height(20));
                }
                if (item.debug)
                {
                    GUILayout.Space(4);
                    EditorGUILayout.BeginVertical(GUILayout.Width(12), GUILayout.Height(20));
                    {
                        GUILayout.Space(3);
                        EzEditorUtility.DrawTexture(EResources.IconPoolyDebugActive, 12, 12);
                        GUILayout.Space(5);
                    }
                    EditorGUILayout.EndVertical();
                }
                if (alignCenter) { GUILayout.FlexibleSpace(); }
                else { GUILayout.Space(4); }
            }
            EditorGUILayout.EndHorizontal();
        }

        /// <summary>
        /// Draws all categories and all the items
        /// </summary>
        void DrawItems()
        {
            float fullWidth = WIDTH_1;
            float indentWidth = WIDTH_1 - INDENT;
            SaveCurrentColorsAndResetColors();
            foreach (var category in categoryItem.Keys)
            {
                if (GUILayout.Button(category,
                                     categoryExpanded[category].target ? barOpenCategory : barClosedCategory,
                                     GUILayout.Width(WIDTH_1)))
                {
                    categoryExpanded[category].target = !categoryExpanded[category].target;
                    if (categoryExpanded[category].target == false)
                    {
                        PoolyEditor.CloseCategoryItems(category, itemExpanded); //if the category is closed, we close all of it's items
                        if (!string.IsNullOrEmpty(renameCategory) && category.Equals(renameCategory)) //if the dev closed this category and it was in rename mode, we get it out of that mode
                        {
                            renameCategory = string.Empty;
                            newCategoryName = string.Empty;
                        }
                    }
                    GUIUtility.keyboardControl = 0;
                }
                if (EditorGUILayout.BeginFadeGroup(categoryExpanded[category].faded))
                {
                    DrawCategoryOptionsButtons(category);
                    EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                    EditorGUILayout.BeginHorizontal(GUILayout.Width(fullWidth));
                    {
                        EzEditorUtility.HorizontalSpace((int)INDENT);
                        EditorGUILayout.BeginVertical(GUILayout.Width(indentWidth - 22));
                        {
                            foreach (var item in categoryItem[category])
                            {
                                DrawItem(item, indentWidth);
                                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
                            }
                        }
                        EditorGUILayout.EndVertical();
                    }
                    EditorGUILayout.EndHorizontal();
                    EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 4);
                }
                EditorGUILayout.EndFadeGroup();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
            }
            LoadPreviousColors();
        }

        /// <summary>
        /// Draws an Item
        /// </summary>
        /// <param name="item"></param>
        /// <param name="width"></param>
        public void DrawItem(Pooly.Item item, float width)
        {
            SaveCurrentColorsAndResetColors();
            if (GUILayout.Button((item.prefab == null) ? "Missing Prefab" : item.prefab.name,
                                                    itemExpanded[item].target ?
                                                    (item.prefab == null) ? barOpenRed : barOpenGrey :
                                                    (item.prefab == null) ? barClosedRed : barClosedGrey,
                                                    GUILayout.Width(width - 22)))
            {
                itemExpanded[item].target = !itemExpanded[item].target;
                GUIUtility.keyboardControl = 0;
            }
            GUILayout.Space(-26);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(width));
            {
                GUILayout.Space(width - 20);
                if (GUILayout.Button("x", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(20), GUILayout.Height(20)))
                {
                    DeleteItem(item);
                    GUIUtility.keyboardControl = 0;
                }
            }
            EditorGUILayout.EndHorizontal();
            if (item.prefab != null)
            {
                GUILayout.Space(-20);
                DrawItemQuickViewInfo(item, width - 2);
            }
            EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            if (EditorGUILayout.BeginFadeGroup(itemExpanded[item].faded))
            {
                GUILayout.Space(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width));
                {
                    GUILayout.Space(4);
                    if (GUILayout.Button("", skin.GetStyle(EzStyle.StyleName.BtnGreyMild.ToString()), GUILayout.Width(40), GUILayout.Height(40)))
                    {
                        if (item.prefab != null) { EditorGUIUtility.PingObject(item.prefab.gameObject); }
                        else { Debug.Log("[Pooly] Missing prefab. Please link a prefab to the pool object."); }
                        GUIUtility.keyboardControl = 0;
                    }
                }
                EditorGUILayout.EndHorizontal();
                GUILayout.Space(-41);
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.Space(5);
                    Texture2D assetPreview = item.prefab == null ? EzResources.BtnRedNormal as Texture2D : AssetPreview.GetAssetPreview(item.prefab.gameObject) as Texture2D;
                    EditorGUILayout.LabelField(new GUIContent(assetPreview), GUILayout.Width(38), GUILayout.Height(38));
                }
                EditorGUILayout.EndHorizontal();
                GUILayout.Space(-43);
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.Space(47);
                    EditorGUILayout.BeginVertical(GUILayout.Width(width - 47 - 22));
                    {
                        EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 47 - 22));
                        {
                            EditorGUILayout.LabelField("Number of Clones", GUILayout.Width(108));
                            GUILayout.Space(18);
                            if (item.prefab == null) { SaveCurrentColorsAndResetColors(); EzEditorUtility.SetBackgroundColor(EzColors.L_RED); }
                            if (item.prefab != null || item.prefab == null && itemExpanded[item].faded > 0.1f)
                            {
                                item.prefab = EditorGUILayout.ObjectField(GUIContent.none, item.prefab, typeof(Transform), false, GUILayout.Width(194)) as Transform;
                            }
                            if (item.prefab == null) { LoadPreviousColors(); }
                            GUILayout.FlexibleSpace();
                        }
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 47 - 22));
                        {
                            GUILayout.Space(6);
                            EditorGUILayout.BeginVertical(GUILayout.Width(14), GUILayout.Height(EditorGUIUtility.singleLineHeight));
                            {
                                GUILayout.FlexibleSpace();
                                EzEditorUtility.DrawTexture(EResources.IconPoolyPreloadNumberActive, 14, 14);
                            }
                            EditorGUILayout.EndVertical();
                            item.preloadCloneCount = EditorGUILayout.IntField(item.preloadCloneCount, GUILayout.Width(86));
                            if (item.preloadCloneCount < 1) { item.preloadCloneCount = 1; }
                            GUILayout.Space(20);
                            if (changeCategoryItem != item)
                            {
                                if (GUILayout.Button("Change Category", skin.GetStyle(EzStyle.StyleName.BtnGreyMild.ToString()), GUILayout.Width(174)))
                                {
                                    changeCategoryItem = item;
                                    changeCategoryItemIndex = categoriesList.FindIndex(x => x.Equals(item.category));
                                    newCategoryName = string.Empty;
                                }
                            }
                            else
                            {
                                if (categoriesList[changeCategoryItemIndex].Equals(PoolyEditor.NEW_CATEGORY))
                                {
                                    changeCategoryNewCategory = EditorGUILayout.TextField(changeCategoryNewCategory, GUILayout.Width(120));
                                }
                                else
                                {
                                    changeCategoryItemIndex = EditorGUILayout.Popup(changeCategoryItemIndex, categoriesList.ToArray(), GUILayout.Width(120));
                                }
                                if (GUILayout.Button("ok", skin.GetStyle(EzStyle.StyleName.BtnGreen.ToString()), GUILayout.Width(42)))
                                {
                                    if (categoriesList[changeCategoryItemIndex].Equals(PoolyEditor.NEW_CATEGORY)) //create new 'change to' category
                                    {
                                        if (!item.category.Equals(changeCategoryNewCategory))
                                        {
                                            if (string.IsNullOrEmpty(changeCategoryNewCategory.Trim())) { EditorUtility.DisplayDialog("Change Item Category", "Cannot change the item's category to a category with no name.", "Ok"); }
                                            else if (changeCategoryNewCategory.Trim().Equals(PoolyEditor.NEW_CATEGORY)) { EditorUtility.DisplayDialog("Change Item Category", "Cannot create a new category named '" + PoolyEditor.NEW_CATEGORY + "'.", "Ok"); }
                                            else if (changeCategoryNewCategory.Trim().Equals(Pooly.DEFAULT_CATEGORY_NAME)) { EditorUtility.DisplayDialog("Change Item Category", "Cannot create a new category named '" + Pooly.DEFAULT_CATEGORY_NAME + "'.", "Ok"); }
                                            else if (!item.category.Equals(changeCategoryNewCategory)) //check that we do not move to the same category (case where we do nothing)
                                            {
                                                if (!categoriesList.Contains(changeCategoryNewCategory)) //this is a new category name
                                                {
                                                    categoriesList.Add(changeCategoryNewCategory);
                                                    categories.Add(changeCategoryNewCategory);
                                                    categoryItem.Add(changeCategoryNewCategory, new List<Pooly.Item> { item });
                                                    categoryExpanded.Add(changeCategoryNewCategory, new AnimBool(false, Repaint));
                                                }
                                                else
                                                {
                                                    categoryItem[changeCategoryNewCategory].Add(item);
                                                }
                                                categoryExpanded[changeCategoryNewCategory].target = true;
                                                categoryItem[item.category].Remove(item);
                                                if (categoryItem[item.category].Count == 0) //was this the last item in the category
                                                {
                                                    categoryItem.Remove(item.category);
                                                    categoryExpanded.Remove(item.category);
                                                    categories.Remove(item.category);
                                                }
                                                item.category = changeCategoryNewCategory;
                                            }
                                        }
                                    }
                                    else if (!item.category.Equals(categoriesList[changeCategoryItemIndex])) //check that we do not move to the same category (case where we do nothing)
                                    {
                                        if (!categoryItem.ContainsKey(categoriesList[changeCategoryItemIndex]))
                                        {
                                            categoryItem.Add(categoriesList[changeCategoryItemIndex], new List<Pooly.Item> { item });
                                            categoryExpanded.Add(categoriesList[changeCategoryItemIndex], new AnimBool(false, Repaint));
                                        }
                                        else
                                        {
                                            categoryItem[categoriesList[changeCategoryItemIndex]].Add(item);
                                        }
                                        categoryExpanded[categoriesList[changeCategoryItemIndex]].target = true;
                                        categoryItem[item.category].Remove(item);
                                        if (categoryItem[item.category].Count == 0) //was this the last item in the category
                                        {
                                            categoryItem.Remove(item.category);
                                            categoryExpanded.Remove(item.category);
                                        }
                                        item.category = categoriesList[changeCategoryItemIndex];
                                    }
                                    changeCategoryItem = null;
                                    GUIUtility.keyboardControl = 0;
                                    categoryExpandedBeforeUpdate = categoryExpanded;
                                    itemExpandedBeforeUpdate = itemExpanded;
                                    UpdateCategoriesAndItems();
                                    EditorGUIUtility.ExitGUI();
                                }
                                GUILayout.Space(2);
                                if (GUILayout.Button("cancel", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(42)))
                                {
                                    if (categoriesList[changeCategoryItemIndex].Equals(PoolyEditor.NEW_CATEGORY))
                                    {
                                        changeCategoryItemIndex = categoriesList.FindIndex(x => x.Equals(item.category));
                                    }
                                    else
                                    {
                                        changeCategoryItem = null;
                                    }
                                    GUIUtility.keyboardControl = 0;
                                }
                            }
                        }
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.EndVertical();
                }
                EditorGUILayout.EndHorizontal();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS * 2);
                DrawItemSettings(item, width);
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            }
            EditorGUILayout.EndFadeGroup();

            if (item.prefab == null && itemExpanded[item].faded < 0.2f)
            {
                SaveCurrentColorsAndResetColors();
                EzEditorUtility.SetBackgroundColor(EzColors.L_RED);
                GUILayout.Space(-EditorGUIUtility.singleLineHeight - 8 - EditorGUIUtility.singleLineHeight * itemExpanded[item].faded);
                EditorGUILayout.BeginHorizontal(GUILayout.Width(width - 22));
                {
                    GUILayout.FlexibleSpace();
                    item.prefab = EditorGUILayout.ObjectField(GUIContent.none, item.prefab, typeof(Transform), false, GUILayout.Width(194)) as Transform;
                    GUILayout.Space(6 * (1 - itemExpanded[item].faded));
                }
                EditorGUILayout.EndHorizontal();
                LoadPreviousColors();
                EzEditorUtility.VerticalSpace(VERTICAL_SPACE_BETWEEN_ELEMENTS);
            }
            LoadPreviousColors();
        }
    }
}
