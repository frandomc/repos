// Copyright (c) 2016 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using UnityEditor;
using UnityEngine;

namespace Ez.Pooly
{
    public class PoolyWindow : EzWindow
    {
        private static bool _utility = true;
        private static string _title = "Pooly - Professional Pooling System";
        private static bool _focus = true;

        private static float _minWidth = 512;
        private static float _minHeight = 512;

        Pooly GetPooly { get { return FindObjectOfType<Pooly>(); } }
        bool SceneHasPooly { get { return GetPooly != null; } }

        PoolyExtension GetPoolyExtension { get { return FindObjectOfType<PoolyExtension>(); } }
        bool SceneHasPoolyExtension { get { return GetPoolyExtension != null; } }

        [MenuItem("Tools/Ez/Pooly", false, 110)]
        static void Init()
        {
            GetWindow<PoolyWindow>(_utility, _title, _focus);
        }

        void OnEnable()
        {
            repaintOn = RepaintOn.Update;
            showHelp = false;
            SetupWindow();
        }

        void SetupWindow()
        {
            titleContent = new GUIContent(_title);
            minSize = new Vector2(_minWidth, _minHeight);
            maxSize = minSize;
        }

        void OnGUI()
        {
            EzEditorUtility.DrawTexture(EzResources.GeneralBackground, _minWidth, _minHeight);
            GUILayout.Space(-_minHeight);
            GUILayout.Space(32);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(512), GUILayout.Height(128));
            {
                GUILayout.FlexibleSpace();
                EzEditorUtility.DrawTexture(EResources.IconPooly, 128, 128);
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(8);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(512), GUILayout.Height(36));
            {
                GUILayout.Space(192);
                EzEditorUtility.DrawTexture(EResources.IconPoolyTitle, 128, 36);
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(36);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(512), GUILayout.Height(148));
            {
                GUILayout.FlexibleSpace();
                EditorGUILayout.BeginVertical(GUILayout.Width(256), GUILayout.Height(60));
                {
                    EzEditorUtility.DrawTexture(EResources.BarPoolyMainPool, 240, 36);
                    EditorGUILayout.BeginHorizontal(GUILayout.Height(24));
                    {
                        GUILayout.FlexibleSpace();
                        if (!SceneHasPooly)
                        {
                            if (GUILayout.Button("Add to Scene", skin.GetStyle(EzStyle.StyleName.BtnGreen.ToString()), GUILayout.Width(220), GUILayout.Height(24)))
                            {
                                EzEditorUtility.CreateGameObjectFromPrefab(FileHelper.GetRelativeFolderPath("Ez") + "/Pooly/Prefabs/", "Pooly", "Pooly");
                                Repaint();
                            }
                        }
                        else
                        {
                            if (GUILayout.Button("Remove from Scene", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(220), GUILayout.Height(24)))
                            {
                                Undo.DestroyObjectImmediate(GetPooly.gameObject);
                            }
                        }
                        GUILayout.FlexibleSpace();
                    }
                    EditorGUILayout.EndHorizontal();
                    GUILayout.Space(16);
                    EzEditorUtility.DrawTexture(EResources.BarPoolyPoolExtension, 240, 36);
                    EditorGUILayout.BeginHorizontal(GUILayout.Height(24));
                    {
                        GUILayout.FlexibleSpace();
                        if (!SceneHasPoolyExtension)
                        {
                            if (GUILayout.Button("Add to Scene", skin.GetStyle(EzStyle.StyleName.BtnBlue.ToString()), GUILayout.Width(220), GUILayout.Height(24)))
                            {
                                EzEditorUtility.CreateGameObjectFromPrefab(FileHelper.GetRelativeFolderPath("Ez") + "/Pooly/Prefabs/", "PoolyExtension", "PoolyExtension");
                                Repaint();
                            }
                        }
                        else
                        {
                            if (GUILayout.Button("Remove from Scene", skin.GetStyle(EzStyle.StyleName.BtnRed.ToString()), GUILayout.Width(220), GUILayout.Height(24)))
                            {
                                Undo.DestroyObjectImmediate(GetPoolyExtension.gameObject);
                            }
                        }
                        GUILayout.FlexibleSpace();
                    }
                    EditorGUILayout.EndHorizontal();

                }
                EditorGUILayout.EndVertical();
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
            GUILayout.Space(36);
            EditorGUILayout.BeginHorizontal(GUILayout.Width(512), GUILayout.Height(64));
            {
                GUILayout.FlexibleSpace();
                EzEditorUtility.DrawTexture(EzResources.IconEz, 64, 64);
                GUILayout.FlexibleSpace();
            }
            EditorGUILayout.EndHorizontal();
        }
    }
}
