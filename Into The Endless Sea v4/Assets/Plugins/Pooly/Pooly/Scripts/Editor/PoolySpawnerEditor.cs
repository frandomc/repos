// Copyright (c) 2016-2017 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using UnityEditor.AnimatedValues;

namespace Ez.Pooly
{
    [CustomEditor(typeof(PoolySpawner))]
    public class PoolySpawnerEditor : EBaseEditor
    {
        PoolySpawner poolySpawner { get { return (PoolySpawner)target; } }

        SerializedProperty
            autoStart, despawnWhenFinished, spawnStartDelay,
            spawnCount, spawnForever,
            spawnInterval, spawnAtRandomIntervals, spawnAtRandomIntervalMinimum, spawnAtRandomIntervalMaximum,
            useSpawnerAsSpawnLocation,
            prefabSpawnType, spawnAt, locationSpawnType,
            showOnSpawnStarted, OnSpawnStarted,
            showOnSpawnStopped, OnSpawnStopped,
            showOnSpawnResumed, OnSpawnResumed,
            showOnSpawnPaused, OnSpawnPaused,
            showOnSpawnFinished, OnSpawnFinished;

        AnimBool
            animBoolDespawnWhenFinished,
            animBoolShowSpawnLocationsOptions,
            animBoolShowOnSpawnStarted, animBoolShowOnSpawnStopped, animBoolShowOnSpawnResumed, animBoolShowOnSpawnPaused, animBoolShowOnSpawnFinished;

        Color defaultHandlesColor;

        void SerializedObjectFindProperties()
        {
            autoStart = serializedObject.FindProperty("autoStart");
            despawnWhenFinished = serializedObject.FindProperty("despawnWhenFinished");
            spawnStartDelay = serializedObject.FindProperty("spawnStartDelay");
            spawnCount = serializedObject.FindProperty("spawnCount");
            spawnForever = serializedObject.FindProperty("spawnForever");
            spawnInterval = serializedObject.FindProperty("spawnInterval");
            spawnAtRandomIntervals = serializedObject.FindProperty("spawnAtRandomIntervals");
            spawnAtRandomIntervalMinimum = serializedObject.FindProperty("spawnAtRandomIntervalMinimum");
            spawnAtRandomIntervalMaximum = serializedObject.FindProperty("spawnAtRandomIntervalMaximum");
            useSpawnerAsSpawnLocation = serializedObject.FindProperty("useSpawnerAsSpawnLocation");
            prefabSpawnType = serializedObject.FindProperty("prefabSpawnType");
            spawnAt = serializedObject.FindProperty("spawnAt");
            locationSpawnType = serializedObject.FindProperty("locationSpawnType");
            showOnSpawnStarted = serializedObject.FindProperty("showOnSpawnStarted");
            OnSpawnStarted = serializedObject.FindProperty("OnSpawnStarted");
            showOnSpawnStopped = serializedObject.FindProperty("showOnSpawnStopped");
            OnSpawnStopped = serializedObject.FindProperty("OnSpawnStopped");
            showOnSpawnResumed = serializedObject.FindProperty("showOnSpawnResumed");
            OnSpawnResumed = serializedObject.FindProperty("OnSpawnResumed");
            showOnSpawnPaused = serializedObject.FindProperty("showOnSpawnPaused");
            OnSpawnPaused = serializedObject.FindProperty("OnSpawnPaused");
            showOnSpawnFinished = serializedObject.FindProperty("showOnSpawnFinished");
            OnSpawnFinished = serializedObject.FindProperty("OnSpawnFinished");
        }

        void GenerateInfoMessages()
        {
            info = new Dictionary<string, EGUI.InfoMessage>();
            info.Add("AutoStartNever", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "Auto Start Never", message = "The spawner will not automatically start spawning.\nIt's your responsability to start the spawning cycle for this spawner." });
            info.Add("SpawnForever", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "Spawn Forever", message = "The spawner will spawn prefabs forever and it will stop only when you call either StopSpawn or PauseSpawn methods.\nIt's your responsability to stop or pause the spawning cycle for this spawner." });
            info.Add("NoSpawnPoint", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "", message = "Set at least one spawn point by referencing a Transform from the scene or set Use Spawner As Spawn Location as true." });
            warning = new Dictionary<string, EGUI.InfoMessage>();
            warning.Add("NoPrefabs", new EGUI.InfoMessage { show = new AnimBool(false, Repaint), title = "SPAWNER IS DISABLED", message = "Reference at least one prefab." });
        }

        void InitAnimBools()
        {
            animBoolDespawnWhenFinished = new AnimBool(autoStart.enumValueIndex == (int)PoolySpawner.AutoStart.OnSpawned, Repaint);
            animBoolShowSpawnLocationsOptions = new AnimBool(useSpawnerAsSpawnLocation.boolValue, Repaint);
            animBoolShowOnSpawnStarted = new AnimBool(showOnSpawnStarted.boolValue, Repaint);
            animBoolShowOnSpawnStopped = new AnimBool(showOnSpawnStopped.boolValue, Repaint);
            animBoolShowOnSpawnResumed = new AnimBool(showOnSpawnResumed.boolValue, Repaint);
            animBoolShowOnSpawnPaused = new AnimBool(showOnSpawnPaused.boolValue, Repaint);
            animBoolShowOnSpawnFinished = new AnimBool(showOnSpawnFinished.boolValue, Repaint);
        }

        void GetInitialHandlesColors()
        {
            defaultHandlesColor = Handles.color;
        }

        void OnEnable()
        {
            SerializedObjectFindProperties();
            GenerateInfoMessages();
            InitAnimBools();
            GetInitialHandlesColors();
        }

        public static Rect GetScreenRect(Vector2 screenPoint, float width, float height)
        {
            Rect r = new Rect();
            r.x = EditorGUIUtility.ScreenToGUIPoint(screenPoint).x;
            r.y = EditorGUIUtility.ScreenToGUIPoint(screenPoint).y;
            r.width = width;
            r.height = height;
            return r;
        }

        private void OnSceneGUI()
        {
            if (useSpawnerAsSpawnLocation.boolValue) { return; }
            if (!PoolySpawner.PoolySettings.showDottedLines) { return; }
            switch (poolySpawner.spawnAt)
            {
                case PoolySpawner.SpawnAt.Position:
                    if (poolySpawner.spawnPositions != null && poolySpawner.spawnPositions.Count > 0)
                    {
                        for (int i = 0; i < poolySpawner.spawnPositions.Count; i++)
                        {
                            Handles.color = PoolySpawner.PoolySettings.dottedLinesColor;
                            Handles.DrawDottedLine(poolySpawner.transform.position, poolySpawner.spawnPositions[i].spawnPosition, PoolySpawner.PoolySettings.dottedLinesScreenSpaceSize);
                            Handles.color = defaultHandlesColor;
                            if (PoolySpawner.PoolySettings.showLabels)
                            {
                                Handles.Label(new Vector3(poolySpawner.spawnPositions[i].spawnPosition.x + 0.05f,
                                                         poolySpawner.spawnPositions[i].spawnPosition.y - 0.05f,
                                                         poolySpawner.spawnPositions[i].spawnPosition.z),
                                                         "Spawn Position " + i + " " + ETools.Vector3ToString(poolySpawner.spawnPositions[i].spawnPosition,
                                                         PoolySpawner.PoolySettings.decimalPoins));
                            }
                            EditorGUI.BeginChangeCheck();
                            Vector3 updatedSpawnPosition = Handles.PositionHandle(poolySpawner.spawnPositions[i].spawnPosition, Quaternion.identity);
                            if (EditorGUI.EndChangeCheck())
                            {
                                Undo.RecordObject(poolySpawner, "Changed Spawn Position " + i);
                                poolySpawner.spawnPositions[i].spawnPosition = ETools.Vector3Round(updatedSpawnPosition, PoolySpawner.PoolySettings.decimalPoins);
                            }
                        }
                    }
                    break;
                case PoolySpawner.SpawnAt.Transform:
                    if (poolySpawner.spawnPoints != null && poolySpawner.spawnPoints.Count > 0)
                    {
                        for (int i = 0; i < poolySpawner.spawnPoints.Count; i++)
                        {
                            if (poolySpawner.spawnPoints[i].spawnPoint == null) { continue; }
                            Handles.color = PoolySpawner.PoolySettings.dottedLinesColor;
                            Handles.DrawDottedLine(poolySpawner.transform.position, poolySpawner.spawnPoints[i].spawnPoint.position, PoolySpawner.PoolySettings.dottedLinesScreenSpaceSize);
                            Handles.color = defaultHandlesColor;
                            if (PoolySpawner.PoolySettings.showLabels)
                            {
                                Handles.Label(new Vector3(poolySpawner.spawnPoints[i].spawnPoint.position.x + 0.05f,
                                                          poolySpawner.spawnPoints[i].spawnPoint.position.y - 0.05f,
                                                          poolySpawner.spawnPoints[i].spawnPoint.position.z),
                                                          "Spawn Point " + i + " " + ETools.Vector3ToString(poolySpawner.spawnPoints[i].spawnPoint.position,
                                                          PoolySpawner.PoolySettings.decimalPoins));
                            }
                            EGUI.BeginChangeCheck();
                            Vector3 updatedSpawnPointPosition = Handles.PositionHandle(poolySpawner.spawnPoints[i].spawnPoint.position, Quaternion.identity);
                            if (EGUI.EndChangeCheck())
                            {
                                Undo.RecordObject(poolySpawner, "Moved " + poolySpawner.spawnPoints[i].spawnPoint.name);
                                poolySpawner.spawnPoints[i].spawnPoint.position = ETools.Vector3Round(updatedSpawnPointPosition, PoolySpawner.PoolySettings.decimalPoins);
                            }
                        }
                    }
                    break;
            }
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawHeader(EResources.HeaderBarPoolySpawner);
            DrawMainSettings();
            DrawPrefabs();
            DrawLocations();
            DrawOnSpawnEvents();
            DrawEditorSettings();
            serializedObject.ApplyModifiedProperties();
            EGUI.Space(SPACE_4);
        }

        void DrawMainSettings()
        {
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Auto Start On", 84);
                EGUI.PropertyField(autoStart, 94);
                EGUI.FlexibleSpace();
                if (!(autoStart.enumValueIndex == (int)PoolySpawner.AutoStart.Never))
                {
                    EGUI.Label("Auto Start After", 94);
                    EGUI.PropertyField(spawnStartDelay, 40);
                    if (spawnStartDelay.floatValue < 0) { spawnStartDelay.floatValue = 0; }
                    EGUI.Label("seconds", 50);
                }
            }
            EGUI.EndHorizontal();
            info["AutoStartNever"].show.target = PoolySpawner.PoolySettings.showInfoMessages && autoStart.enumValueIndex == (int)PoolySpawner.AutoStart.Never;
            EGUI.DrawInfoMessage(info["AutoStartNever"], EGUI.InfoMessageType.Info, WIDTH_420);
            animBoolDespawnWhenFinished.target = autoStart.enumValueIndex == (int)PoolySpawner.AutoStart.OnSpawned;
            if (EGUI.BeginFadeGroup(animBoolDespawnWhenFinished.faded))
            {
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.Toggle(despawnWhenFinished);
                    EGUI.Label("Despawn When Finished Spawning", WIDTH_420);
                    EGUI.FlexibleSpace();
                }
                EGUI.EndHorizontal();
            }
            EGUI.EndFadeGroup();
            EGUI.BeginHorizontal(WIDTH_420);
            {
                if (!spawnForever.boolValue)
                {
                    EGUI.Label("Spawn Cycles", 84);
                    EGUI.PropertyField(spawnCount, 94);
                    if (spawnCount.intValue < 0) { spawnCount.intValue = 0; }
                    EGUI.FlexibleSpace();
                }
                EGUI.Toggle(spawnForever);
                EGUI.Label("Spawn Forever", 90);
                if (spawnForever.boolValue) { EGUI.FlexibleSpace(); }
            }
            EGUI.EndHorizontal();
            info["SpawnForever"].show.target = PoolySpawner.PoolySettings.showInfoMessages && spawnForever.boolValue;
            EGUI.DrawInfoMessage(info["SpawnForever"], EGUI.InfoMessageType.Info, WIDTH_420);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                if (spawnAtRandomIntervals.boolValue)
                {
                    EGUI.Toggle(spawnAtRandomIntervals);
                    EGUI.Label("Random Spawn Interval Between", 194);
                    EGUI.FlexibleSpace();
                    EGUI.Label("Min", 22);
                    EGUI.PropertyField(spawnAtRandomIntervalMinimum, 40);
                    if (spawnAtRandomIntervalMinimum.floatValue < 0) { spawnAtRandomIntervalMinimum.floatValue = 0; }
                    EGUI.Label("-", 6);
                    EGUI.Label("Max", 26);
                    EGUI.PropertyField(spawnAtRandomIntervalMaximum, 40);
                    if (spawnAtRandomIntervalMaximum.floatValue < spawnAtRandomIntervalMinimum.floatValue) { spawnAtRandomIntervalMaximum.floatValue = spawnAtRandomIntervalMinimum.floatValue; }
                    EGUI.Label("seconds", 50);
                }
                else
                {
                    EGUI.Label("Spawn Every", 84);
                    EGUI.PropertyField(spawnInterval, 40);
                    EGUI.Label("seconds", 50);
                    EGUI.FlexibleSpace();
                    EGUI.Toggle(spawnAtRandomIntervals);
                    EGUI.Label("Set Random Spawn Interval", 166);
                }
            }
            EGUI.EndHorizontal();
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Toggle(useSpawnerAsSpawnLocation);
                EGUI.Label("Use Spawner As Spawn Location", 200);
            }
            EGUI.EndHorizontal();
        }

        void DrawPrefabs()
        {
            EGUI.Space(SPACE_4);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Prefab" + ((poolySpawner.spawnPrefabs != null && poolySpawner.spawnPrefabs.Count > 1) ? "s" : ""), EStyles.GetStyle(EStyles.TextStyle.ComponentSubtitle), 70, HEIGHT_16);
                EGUI.Space(SPACE_4);
                if (poolySpawner.spawnPrefabs != null && poolySpawner.spawnPrefabs.Count > 1)
                {
                    EGUI.Label("Spawn Type", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 80, HEIGHT_16);
                    EGUI.Popup(prefabSpawnType, 90);
                }
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4);
            if (poolySpawner.spawnPrefabs == null || poolySpawner.spawnPrefabs.Count == 0) { poolySpawner.spawnPrefabs = new List<PoolySpawner.SpawnPrefab> { new PoolySpawner.SpawnPrefab(null) }; }
            warning["NoPrefabs"].show.target = !poolySpawner.HasPrefabs();
            EGUI.DrawInfoMessage(warning["NoPrefabs"], WIDTH_420, EGUI.InfoMessageType.Warning);
            for (int i = 0; i < poolySpawner.spawnPrefabs.Count; i++)
            {
                EGUI.Space(SPACE_2);
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.Label(i.ToString(), EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 12);
                    EGUI.BeginChangeCheck();
                    Transform prefabObject = poolySpawner.spawnPrefabs[i].prefab;
                    prefabObject = (Transform)EGUI.ObjectField(prefabObject, typeof(Transform), false, 160);
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(poolySpawner, "Changed Value For Prefab Slot " + i);
                        poolySpawner.spawnPrefabs[i].prefab = prefabObject;
                    }
                    if (EGUI.ButtonMinus())
                    {
                        if (poolySpawner.spawnPrefabs.Count > 1)
                        {
                            Undo.RecordObject(poolySpawner, "Removed Prefab Slot " + i);
                            poolySpawner.spawnPrefabs.RemoveAt(i);
                        }
                        else if (poolySpawner.spawnPrefabs == null || poolySpawner.spawnPrefabs[0].prefab != null)
                        {
                            Undo.RecordObject(poolySpawner, "Reset Prefab Slot");
                            poolySpawner.spawnPrefabs = new List<PoolySpawner.SpawnPrefab> { new PoolySpawner.SpawnPrefab(null) };
                        }
                        EGUI.SetDirty(poolySpawner);
                        EGUI.ExitGUI();
                    }
                    if (poolySpawner.spawnPrefabs != null && poolySpawner.spawnPrefabs.Count > 1 && prefabSpawnType.enumValueIndex == (int)PoolySpawner.SpawnType.Random)
                    {
                        EGUI.Space(SPACE_8);
                        EGUI.Label("weight", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 40, HEIGHT_16);
                        EGUI.BeginChangeCheck();
                        int spawnPrefabWeight = poolySpawner.spawnPrefabs[i].weight;
                        spawnPrefabWeight = EditorGUILayout.IntSlider(spawnPrefabWeight, 0, 100);
                        if (EGUI.EndChangeCheck())
                        {
                            Undo.RecordObject(poolySpawner, "Changed Weight Of Prefab " + i);
                            poolySpawner.spawnPrefabs[i].weight = spawnPrefabWeight;
                        }
                    }
                    EGUI.FlexibleSpace();
                }
                EGUI.EndHorizontal();
            }
            EGUI.Space(SPACE_2);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Space(176 + SPACE_4);
                if (EGUI.ButtonPlus())
                {
                    Undo.RecordObject(poolySpawner, "Added New Prefab Slot");
                    poolySpawner.spawnPrefabs.Add(new PoolySpawner.SpawnPrefab(null));
                    EGUI.SetDirty(poolySpawner);
                    EGUI.ExitGUI();
                }
            }
            EGUI.EndHorizontal();
        }

        void DrawLocations()
        {
            animBoolShowSpawnLocationsOptions.target = !useSpawnerAsSpawnLocation.boolValue;
            if (EGUI.BeginFadeGroup(animBoolShowSpawnLocationsOptions.faded))
            {
                EGUI.Space(SPACE_4);
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.Label("Spawn At", EStyles.GetStyle(EStyles.TextStyle.ComponentSubtitle), 85, HEIGHT_16);
                    EGUI.Popup(spawnAt, 70);
                    EGUI.Space(SPACE_4);
                    EGUI.Label("Spawn Type", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 80, HEIGHT_16);
                    EGUI.Popup(locationSpawnType, 100);

                    EGUI.FlexibleSpace();
                }
                EGUI.EndHorizontal();
                EGUI.Space(SPACE_4);
                switch (poolySpawner.spawnAt)
                {
                    case PoolySpawner.SpawnAt.Position:
                        if (poolySpawner.spawnPositions == null || poolySpawner.spawnPositions.Count == 0) { poolySpawner.spawnPositions = new List<PoolySpawner.SpawnPosition> { new PoolySpawner.SpawnPosition(new Vector3(0, 1, 0)) }; }
                        for (int i = 0; i < poolySpawner.spawnPositions.Count; i++)
                        {
                            EGUI.Space(SPACE_2);
                            EGUI.BeginHorizontal(WIDTH_420);
                            {
                                EGUI.Label(i.ToString(), EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 12);
                                EGUI.BeginChangeCheck();
                                Vector3 spawnPosition = poolySpawner.spawnPositions[i].spawnPosition;
                                spawnPosition = EGUI.Vector3(spawnPosition, 160);
                                if (EGUI.EndChangeCheck())
                                {
                                    Undo.RecordObject(poolySpawner, "Changed Spawn Position " + i);
                                    poolySpawner.spawnPositions[i].spawnPosition = spawnPosition;
                                    EGUI.SetDirty(poolySpawner);
                                }
                                if (EGUI.ButtonMinus())
                                {
                                    if (poolySpawner.spawnPositions.Count > 1)
                                    {
                                        Undo.RecordObject(poolySpawner, "Removed Spawn Position " + i);
                                        poolySpawner.spawnPositions.RemoveAt(i);
                                    }
                                    else if (poolySpawner.spawnPositions == null || poolySpawner.spawnPositions[0].spawnPosition != new Vector3(poolySpawner.transform.position.x, poolySpawner.transform.position.y + 1, poolySpawner.transform.position.z))
                                    {
                                        Undo.RecordObject(poolySpawner, "Reset Spawn Position");
                                        poolySpawner.spawnPositions = new List<PoolySpawner.SpawnPosition> { new PoolySpawner.SpawnPosition(new Vector3(poolySpawner.transform.position.x, poolySpawner.transform.position.y + 1, poolySpawner.transform.position.z)) };
                                    }
                                    EGUI.SetDirty(poolySpawner);
                                    EGUI.ExitGUI();
                                }
                                if (poolySpawner.spawnPositions != null && poolySpawner.spawnPositions.Count > 1 && locationSpawnType.enumValueIndex == (int)PoolySpawner.SpawnType.Random)
                                {
                                    EGUI.Space(SPACE_8);
                                    EGUI.Label("weight", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 40, HEIGHT_16);
                                    EGUI.BeginChangeCheck();
                                    int spawnPositionWeight = poolySpawner.spawnPositions[i].weight;
                                    spawnPositionWeight = EditorGUILayout.IntSlider(spawnPositionWeight, 0, 100);
                                    if (EGUI.EndChangeCheck())
                                    {
                                        Undo.RecordObject(poolySpawner, "Changed Weight Of Spawn Position " + i);
                                        poolySpawner.spawnPositions[i].weight = spawnPositionWeight;
                                    }
                                }
                                EGUI.FlexibleSpace();
                            }
                            EGUI.EndHorizontal();
                        }
                        EGUI.Space(SPACE_2);
                        EGUI.BeginHorizontal(WIDTH_420);
                        {
                            EGUI.Space(176 + SPACE_4);
                            if (EGUI.ButtonPlus())
                            {
                                Undo.RecordObject(poolySpawner, "Added Spawn Position");
                                poolySpawner.spawnPositions.Add(new PoolySpawner.SpawnPosition(new Vector3(poolySpawner.transform.position.x, poolySpawner.transform.position.y + 1, poolySpawner.transform.position.z)));
                                EGUI.SetDirty(poolySpawner);
                                EGUI.ExitGUI();
                            }
                        }
                        EGUI.EndHorizontal();
                        break;
                    case PoolySpawner.SpawnAt.Transform:
                        if (poolySpawner.spawnPoints == null || poolySpawner.spawnPoints.Count == 0) { poolySpawner.spawnPoints = new List<PoolySpawner.SpawnPoint> { new PoolySpawner.SpawnPoint(null) }; }
                        info["NoSpawnPoint"].show.target = PoolySpawner.PoolySettings.showInfoMessages && !poolySpawner.HasSpawnPoints();
                        EGUI.DrawInfoMessage(info["NoSpawnPoint"], WIDTH_420, EGUI.InfoMessageType.Info);
                        for (int i = 0; i < poolySpawner.spawnPoints.Count; i++)
                        {
                            EGUI.Space(SPACE_2);
                            EGUI.BeginHorizontal(WIDTH_420);
                            {
                                EGUI.Label(i.ToString(), EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 12);
                                EGUI.BeginChangeCheck();
                                Transform spawnPoint = poolySpawner.spawnPoints[i].spawnPoint;
                                spawnPoint = (Transform)EGUI.ObjectField(spawnPoint, typeof(Transform), true, 160);
                                if (EGUI.IsPersistent(spawnPoint)) { spawnPoint = poolySpawner.spawnPoints[i].spawnPoint; }
                                if (EGUI.EndChangeCheck())
                                {
                                    Undo.RecordObject(poolySpawner, "Changed Reference For Spawn Point Slot " + i);
                                    poolySpawner.spawnPoints[i].spawnPoint = spawnPoint;
                                    EGUI.SetDirty(poolySpawner);
                                }
                                if (EGUI.ButtonMinus())
                                {
                                    if (poolySpawner.spawnPoints.Count > 1)
                                    {
                                        Undo.RecordObject(poolySpawner, "Removed Spawn Point Slot " + i);
                                        poolySpawner.spawnPoints.RemoveAt(i);
                                    }
                                    else if (poolySpawner.spawnPoints == null || poolySpawner.spawnPoints[0].spawnPoint != null)
                                    {
                                        Undo.RecordObject(poolySpawner, "Reset Spawn Point Slot");
                                        poolySpawner.spawnPoints = new List<PoolySpawner.SpawnPoint> { new PoolySpawner.SpawnPoint(null) };
                                    }
                                    EGUI.SetDirty(poolySpawner);
                                    EGUI.ExitGUI();
                                }
                                if (poolySpawner.spawnPoints != null && poolySpawner.spawnPoints.Count > 1 && locationSpawnType.enumValueIndex == (int)PoolySpawner.SpawnType.Random)
                                {
                                    EGUI.Space(SPACE_8);
                                    EGUI.Label("weight", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 40, HEIGHT_16);
                                    EGUI.BeginChangeCheck();
                                    int spawnPointWeight = poolySpawner.spawnPoints[i].weight;
                                    spawnPointWeight = EditorGUILayout.IntSlider(spawnPointWeight, 0, 100);
                                    if (EGUI.EndChangeCheck())
                                    {
                                        Undo.RecordObject(poolySpawner, "Changed Weight Of Spawn Point " + i);
                                        poolySpawner.spawnPoints[i].weight = spawnPointWeight;
                                    }
                                }
                                EGUI.FlexibleSpace();
                            }
                            EGUI.EndHorizontal();
                        }
                        EGUI.Space(SPACE_2);
                        EGUI.BeginHorizontal(WIDTH_420);
                        {
                            EGUI.Space(176 + SPACE_4);
                            if (EGUI.ButtonPlus())
                            {
                                Undo.RecordObject(poolySpawner, "Added Spawn Point Slot");
                                poolySpawner.spawnPoints.Add(new PoolySpawner.SpawnPoint(null));
                                EGUI.SetDirty(poolySpawner);
                                EGUI.ExitGUI();
                            }
                        }
                        EGUI.EndHorizontal();
                        break;
                }
            }
            EGUI.EndFadeGroup();
        }

        void DrawOnSpawnEvents()
        {
            EGUI.Space(SPACE_16);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("UnityEvent Callbacks", EStyles.GetStyle(EStyles.TextStyle.ComponentSubtitle), 240, HEIGHT_16);
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_8);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("OnSpawn", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 66, HEIGHT_16);
                EGUI.Space(SPACE_8);
                EGUI.Label("Started", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 36, HEIGHT_16);
                EGUI.Toggle(showOnSpawnStarted);
                EGUI.Space(SPACE_8);
                EGUI.Label("Stopped", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 34, HEIGHT_16);
                EGUI.Toggle(showOnSpawnStopped);
                EGUI.Space(SPACE_8);
                EGUI.Label("Paused", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 32, HEIGHT_16);
                EGUI.Toggle(showOnSpawnPaused);
                EGUI.Space(SPACE_8);
                EGUI.Label("Resumed", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 38, HEIGHT_16);
                EGUI.Toggle(showOnSpawnResumed);
                EGUI.Space(SPACE_8);
                EGUI.Label("Finished", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 38, HEIGHT_16);
                EGUI.Toggle(showOnSpawnFinished);
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4, showOnSpawnStarted.boolValue || showOnSpawnPaused.boolValue || showOnSpawnFinished.boolValue);
            animBoolShowOnSpawnStarted.target = showOnSpawnStarted.boolValue;
            if (EGUI.BeginFadeGroup(animBoolShowOnSpawnStarted.faded))
            {
                EGUI.PropertyField(OnSpawnStarted, "OnSpawnStarted", WIDTH_420);
            }
            EGUI.EndFadeGroup();
            animBoolShowOnSpawnStopped.target = showOnSpawnStopped.boolValue;
            if (EGUI.BeginFadeGroup(animBoolShowOnSpawnStopped.faded))
            {
                EGUI.PropertyField(OnSpawnStopped, "OnSpawnStopped", WIDTH_420);
            }
            EGUI.EndFadeGroup();
            animBoolShowOnSpawnPaused.target = showOnSpawnPaused.boolValue;
            if (EGUI.BeginFadeGroup(animBoolShowOnSpawnPaused.faded))
            {
                EGUI.PropertyField(OnSpawnPaused, "OnSpawnPaused", WIDTH_420);
            }
            EGUI.EndFadeGroup();
            animBoolShowOnSpawnResumed.target = showOnSpawnResumed.boolValue;
            if (EGUI.BeginFadeGroup(animBoolShowOnSpawnResumed.faded))
            {
                EGUI.PropertyField(OnSpawnResumed, "OnSpawnResumed", WIDTH_420);
            }
            EGUI.EndFadeGroup();
            animBoolShowOnSpawnFinished.target = showOnSpawnFinished.boolValue;
            if (EGUI.BeginFadeGroup(animBoolShowOnSpawnFinished.faded))
            {
                EGUI.PropertyField(OnSpawnFinished, "OnSpawnfinished", WIDTH_420);
            }
            EGUI.EndFadeGroup();

        }

        void DrawEditorSettings()
        {
            EGUI.Space(SPACE_16);
            EGUI.Label("Editor Settings", EStyles.GetStyle(EStyles.TextStyle.ComponentSubtitle), WIDTH_420, HEIGHT_16);
            EGUI.Space(SPACE_8);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Icons", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 40, HEIGHT_16);
                EGUI.Space(SPACE_8);
                EGUI.Label("Always Show", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 60, HEIGHT_16);
                EGUI.BeginChangeCheck();
                bool tempAlwaysShowIcons = PoolySpawner.PoolySettings.alwaysShowIcons;
                tempAlwaysShowIcons = EGUI.Toggle(tempAlwaysShowIcons);
                if (EGUI.EndChangeCheck())
                {
                    Undo.RecordObject(this, "Toggled AlwaysShowIcons");
                    PoolySpawner.PoolySettings.alwaysShowIcons = tempAlwaysShowIcons;
                    SceneView.RepaintAll();
                }
                EGUI.Space(SPACE_8);
                EGUI.Label("Spawner", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 40, HEIGHT_16);
                EGUI.BeginChangeCheck();
                bool tempShowSpawnerIcon = PoolySpawner.PoolySettings.showSpawnerIcon;
                tempShowSpawnerIcon = EGUI.Toggle(tempShowSpawnerIcon);
                if (EGUI.EndChangeCheck())
                {
                    Undo.RecordObject(this, "Toggled ShowSpawnerIcon");
                    PoolySpawner.PoolySettings.showSpawnerIcon = tempShowSpawnerIcon;
                    SceneView.RepaintAll();
                }
                EGUI.Space(SPACE_8);
                if (!useSpawnerAsSpawnLocation.boolValue)
                {
                    EGUI.Label("Spawn Locations", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 76, HEIGHT_16);
                    EGUI.BeginChangeCheck();
                    bool tempShowSpawnLocationsIcons = PoolySpawner.PoolySettings.showSpawnLocationsIcons;
                    tempShowSpawnLocationsIcons = EGUI.Toggle(tempShowSpawnLocationsIcons);
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(this, "Toggled ShowSpawnLocationsIcons");
                        PoolySpawner.PoolySettings.showSpawnLocationsIcons = tempShowSpawnLocationsIcons;
                        SceneView.RepaintAll();
                    }
                    EGUI.Space(SPACE_8);
                }
                EGUI.Label("Allow Scaling", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 64, HEIGHT_16);
                EGUI.BeginChangeCheck();
                bool tempAllowIconScaling = PoolySpawner.PoolySettings.allowIconScaling;
                tempAllowIconScaling = EGUI.Toggle(tempAllowIconScaling);
                if (EGUI.EndChangeCheck())
                {
                    Undo.RecordObject(this, "Toggled AllowIconScaling");
                    PoolySpawner.PoolySettings.allowIconScaling = tempAllowIconScaling;
                    SceneView.RepaintAll();
                }
                EGUI.Space(SPACE_8, !useSpawnerAsSpawnLocation.boolValue);
                EGUI.FlexibleSpace(useSpawnerAsSpawnLocation.boolValue);
                if (EGUI.ButtonReset())
                {
                    Undo.RecordObject(this, "Reset icons settings");
                    PoolySpawner.PoolySettings.ResetIconsSetting();
                    SceneView.RepaintAll();
                }
            }
            EGUI.EndHorizontal();
            if (EGUI.BeginFadeGroup(animBoolShowSpawnLocationsOptions.faded))
            {
                EGUI.Space(SPACE_4);
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.Label("Labels", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 40, HEIGHT_16);
                    EGUI.Space(SPACE_8);
                    EGUI.Label("Show", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 26, HEIGHT_16);
                    EGUI.BeginChangeCheck();
                    bool tempShowLabels = PoolySpawner.PoolySettings.showLabels;
                    tempShowLabels = EGUI.Toggle(tempShowLabels);
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(this, "Toggled ShowLabels");
                        PoolySpawner.PoolySettings.showLabels = tempShowLabels;
                        SceneView.RepaintAll();
                    }
                    EGUI.FlexibleSpace();
                    EGUI.Label("Decimal Points", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 76, HEIGHT_16);
                    EGUI.Space(-SPACE_2);
                    EGUI.BeginChangeCheck();
                    int tempDecimalPoins = PoolySpawner.PoolySettings.decimalPoins;
                    tempDecimalPoins = EditorGUILayout.IntSlider(tempDecimalPoins, 0, 4, GUILayout.Width(156));
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(this, "Changed DecimalPoins");
                        PoolySpawner.PoolySettings.decimalPoins = tempDecimalPoins;
                        SceneView.RepaintAll();
                    }
                    if (EGUI.ButtonReset())
                    {
                        Undo.RecordObject(this, "Reset label settings");
                        PoolySpawner.PoolySettings.ResetLabelSettings();
                        SceneView.RepaintAll();
                    }

                }
                EGUI.EndHorizontal();
                EGUI.Space(SPACE_4);
                EGUI.BeginHorizontal(WIDTH_420);
                {
                    EGUI.Label("Dotted Lines", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 90, HEIGHT_16);
                    EGUI.Space(SPACE_4);
                    EGUI.Label("Show", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 26, HEIGHT_16);
                    EGUI.BeginChangeCheck();
                    bool tempShowDottedLines = PoolySpawner.PoolySettings.showDottedLines;
                    tempShowDottedLines = EGUI.Toggle(tempShowDottedLines);
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(this, "Toggled dotted lines visiblility");
                        PoolySpawner.PoolySettings.showDottedLines = tempShowDottedLines;
                        SceneView.RepaintAll();
                    }
                    EGUI.Space(SPACE_8);
                    EGUI.Label("Color", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 30, HEIGHT_16);
                    EGUI.BeginChangeCheck();
                    Color tempLinesColor = PoolySpawner.PoolySettings.dottedLinesColor;
                    tempLinesColor = EGUI.ColorField(tempLinesColor, false, false, true, null, 20);
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(this, "Changed dotted lines color");
                        PoolySpawner.PoolySettings.dottedLinesColor = tempLinesColor;
                        SceneView.RepaintAll();
                    }
                    EGUI.Space(SPACE_8);
                    EGUI.Label("Size", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 24, HEIGHT_16);
                    EGUI.Space(-SPACE_2);
                    EGUI.BeginChangeCheck();
                    float tempDottedLineScreenSpaceSize = PoolySpawner.PoolySettings.dottedLinesScreenSpaceSize;
                    tempDottedLineScreenSpaceSize = EditorGUILayout.Slider(tempDottedLineScreenSpaceSize, 1, 10);
                    if (EGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(this, "Changed dotted lines size");
                        PoolySpawner.PoolySettings.dottedLinesScreenSpaceSize = tempDottedLineScreenSpaceSize;
                        SceneView.RepaintAll();
                    }
                    if (EGUI.ButtonReset())
                    {
                        Undo.RecordObject(this, "Reset dotted lines settings");
                        PoolySpawner.PoolySettings.ResetDottedLinesSettings();
                        SceneView.RepaintAll();
                    }
                }
                EGUI.EndHorizontal();
            }
            EGUI.EndFadeGroup();
            EGUI.Space(SPACE_4);
            EGUI.BeginHorizontal(WIDTH_420);
            {
                EGUI.Label("Messages", EStyles.GetStyle(EStyles.TextStyle.ComponentNormal), 60, HEIGHT_16);
                EGUI.Space(SPACE_8);
                EGUI.Label("Show Info Messages", EStyles.GetStyle(EStyles.TextStyle.ComponentSmall), 90, HEIGHT_16);
                EGUI.BeginChangeCheck();
                bool tempShowInfoMessages = PoolySpawner.PoolySettings.showInfoMessages;
                tempShowInfoMessages = EGUI.Toggle(tempShowInfoMessages);
                if (EGUI.EndChangeCheck())
                {
                    Undo.RecordObject(this, "Toggled ShowInfoMessages");
                    PoolySpawner.PoolySettings.showInfoMessages = tempShowInfoMessages;
                    SceneView.RepaintAll();
                }
                EGUI.FlexibleSpace();
            }
            EGUI.EndHorizontal();
            EGUI.Space(SPACE_4);
        }
    }
}
