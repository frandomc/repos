// Copyright (c) 2016 Ez Entertainment SRL. All Rights Reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms

using UnityEngine;

namespace Ez
{
    public partial class EResources
    {
        private static string pathPoolyImages;
        public static string PathPoolyImages { get { if (string.IsNullOrEmpty(pathPoolyImages)) pathPoolyImages = FileHelper.GetRelativeFolderPath("Ez") + "/Pooly/Images/"; return pathPoolyImages; } }

        //h_bar_pooly @420x36
        private static Texture headerBarPooly;
        public static Texture HeaderBarPooly { get { if (headerBarPooly == null) headerBarPooly = EGUI.GetTexture("h_bar_pooly", PathPoolyImages) as Texture; return headerBarPooly; } }

        //h_bar_pooly_extension @420x36
        private static Texture headerBarPoolyExtension;
        public static Texture HeaderBarPoolyExtension { get { if (headerBarPoolyExtension == null) headerBarPoolyExtension = EGUI.GetTexture("h_bar_pooly_extension", PathPoolyImages) as Texture; return headerBarPoolyExtension; } }

        //h_bar_pooly_despawner_time
        private static Texture headerBarPoolyDespawnerTime;
        public static Texture HeaderBarPoolyDespawnerTime { get { if (headerBarPoolyDespawnerTime == null) headerBarPoolyDespawnerTime = EGUI.GetTexture("h_bar_pooly_despawner_time", PathPoolyImages) as Texture; return headerBarPoolyDespawnerTime; } }

        //h_bar_pooly_despawner_sound
        private static Texture headerBarPoolyDespawnerSound;
        public static Texture HeaderBarPoolyDespawnerSound { get { if (headerBarPoolyDespawnerSound == null) headerBarPoolyDespawnerSound = EGUI.GetTexture("h_bar_pooly_despawner_sound", PathPoolyImages) as Texture; return headerBarPoolyDespawnerSound; } }

        //h_bar_pooly_despawner_effect
        private static Texture headerBarPoolyDespawnerEffect;
        public static Texture HeaderBarPoolyDespawnerEffect { get { if (headerBarPoolyDespawnerEffect == null) headerBarPoolyDespawnerEffect = EGUI.GetTexture("h_bar_pooly_despawner_effect", PathPoolyImages) as Texture; return headerBarPoolyDespawnerEffect; } }

        //h_bar_pooly_despawner_collision
        private static Texture headerBarPoolyDespawnerCollision;
        public static Texture HeaderBarPoolyDespawnerCollision { get { if (headerBarPoolyDespawnerCollision == null) headerBarPoolyDespawnerCollision = EGUI.GetTexture("h_bar_pooly_despawner_collision", PathPoolyImages) as Texture; return headerBarPoolyDespawnerCollision; } }

        //h_bar_pooly_despawner_trigger
        private static Texture headerBarPoolyDespawnerTrigger;
        public static Texture HeaderBarPoolyDespawnerTrigger { get { if (headerBarPoolyDespawnerTrigger == null) headerBarPoolyDespawnerTrigger = EGUI.GetTexture("h_bar_pooly_despawner_trigger", PathPoolyImages) as Texture; return headerBarPoolyDespawnerTrigger; } }

        //h_bar_pooly_despawner_collision2d
        private static Texture headerBarPoolyDespawnerCollision2D;
        public static Texture HeaderBarPoolyDespawnerCollision2D { get { if (headerBarPoolyDespawnerCollision2D == null) headerBarPoolyDespawnerCollision2D = EGUI.GetTexture("h_bar_pooly_despawner_collision2d", PathPoolyImages) as Texture; return headerBarPoolyDespawnerCollision2D; } }


        //h_bar_pooly_despawner_trigger2d
        private static Texture headerBarPoolyDespawnerTrigger2D;
        public static Texture HeaderBarPoolyDespawnerTrigger2D { get { if (headerBarPoolyDespawnerTrigger2D == null) headerBarPoolyDespawnerTrigger2D = EGUI.GetTexture("h_bar_pooly_despawner_trigger2d", PathPoolyImages) as Texture; return headerBarPoolyDespawnerTrigger2D; } }

        //h_bar_pooly_spawner
        private static Texture headerBarPoolySpawner;
        public static Texture HeaderBarPoolySpawner { get { if (headerBarPoolySpawner == null) headerBarPoolySpawner = EGUI.GetTexture("h_bar_pooly_spawner", PathPoolyImages) as Texture; return headerBarPoolySpawner; } }

        //icon_pooly_instantiate_more @14x14
        private static Texture iconPoolyInstantiateMore, iconPoolyInstantiateMoreActive;
        public static Texture IconPoolyInstantiateMore { get { if (iconPoolyInstantiateMore == null) iconPoolyInstantiateMore = EGUI.GetTexture("icon_pooly_instantiate_more", PathPoolyImages) as Texture; return iconPoolyInstantiateMore; } }
        public static Texture IconPoolyInstantiateMoreActive { get { if (iconPoolyInstantiateMoreActive == null) iconPoolyInstantiateMoreActive = EGUI.GetTexture("icon_pooly_instantiate_more_active", PathPoolyImages) as Texture; return iconPoolyInstantiateMoreActive; } }

        //icon_pooly_max @20x20
        private static Texture iconPoolyMax, iconPoolyMaxActive;
        public static Texture IconPoolyMax { get { if (iconPoolyMax == null) iconPoolyMax = EGUI.GetTexture("icon_pooly_max", PathPoolyImages) as Texture; return iconPoolyMax; } }
        public static Texture IconPoolyMaxActive { get { if (iconPoolyMaxActive == null) iconPoolyMaxActive = EGUI.GetTexture("icon_pooly_max_active", PathPoolyImages) as Texture; return iconPoolyMaxActive; } }

        //icon_pooly_preload_number @14x14
        private static Texture iconPoolyPreloadNumber, iconPoolyPreloadNumberActive;
        public static Texture IconPoolyPreloadNumber { get { if (iconPoolyPreloadNumber == null) iconPoolyPreloadNumber = EGUI.GetTexture("icon_pooly_preload_number", PathPoolyImages) as Texture; return iconPoolyPreloadNumber; } }
        public static Texture IconPoolyPreloadNumberActive { get { if (iconPoolyPreloadNumberActive == null) iconPoolyPreloadNumberActive = EGUI.GetTexture("icon_pooly_preload_number_active", PathPoolyImages) as Texture; return iconPoolyPreloadNumberActive; } }

        //icon_pooly_recycle @14x14
        private static Texture iconPoolyRecycle, iconPoolyRecycleActive;
        public static Texture IconPoolyRecycle { get { if (iconPoolyRecycle == null) iconPoolyRecycle = EGUI.GetTexture("icon_pooly_recycle", PathPoolyImages) as Texture; return iconPoolyRecycle; } }
        public static Texture IconPoolyRecycleActive { get { if (iconPoolyRecycleActive == null) iconPoolyRecycleActive = EGUI.GetTexture("icon_pooly_recycle_active", PathPoolyImages) as Texture; return iconPoolyRecycleActive; } }

        //icon_pooly_wait @14x14
        private static Texture iconPoolyWait, iconPoolyWaitActive;
        public static Texture IconPoolyWait { get { if (iconPoolyWait == null) iconPoolyWait = EGUI.GetTexture("icon_pooly_wait", PathPoolyImages) as Texture; return iconPoolyWait; } }
        public static Texture IconPoolyWaitActive { get { if (iconPoolyWaitActive == null) iconPoolyWaitActive = EGUI.GetTexture("icon_pooly_wait_active", PathPoolyImages) as Texture; return iconPoolyWaitActive; } }

        //icon_pooly_debug @14x14
        private static Texture iconPoolyDebug, iconPoolyDebugActive;
        public static Texture IconPoolyDebug { get { if (iconPoolyDebug == null) iconPoolyDebug = EGUI.GetTexture("icon_pooly_debug", PathPoolyImages) as Texture; return iconPoolyDebug; } }
        public static Texture IconPoolyDebugActive { get { if (iconPoolyDebugActive == null) iconPoolyDebugActive = EGUI.GetTexture("icon_pooly_debug_active", PathPoolyImages) as Texture; return iconPoolyDebugActive; } }

        //icon_pooly @128x128
        private static Texture iconPooly;
        public static Texture IconPooly { get { if (iconPooly == null) iconPooly = EGUI.GetTexture("icon_pooly", PathPoolyImages) as Texture; return iconPooly; } }

        //icon_pooly_title @128x36
        private static Texture iconPoolyTitle;
        public static Texture IconPoolyTitle { get { if (iconPoolyTitle == null) iconPoolyTitle = EGUI.GetTexture("icon_pooly_title", PathPoolyImages) as Texture; return iconPoolyTitle; } }

        //bar_pooly_main_pool @240x36
        private static Texture barPoolyMainPool;
        public static Texture BarPoolyMainPool { get { if (barPoolyMainPool == null) barPoolyMainPool = EGUI.GetTexture("bar_pooly_main_pool", PathPoolyImages) as Texture; return barPoolyMainPool; } }

        //bar_pooly_pool_extension @240x36
        private static Texture barPoolyPoolExtension;
        public static Texture BarPoolyPoolExtension { get { if (barPoolyPoolExtension == null) barPoolyPoolExtension = EGUI.GetTexture("bar_pooly_pool_extension", PathPoolyImages) as Texture; return barPoolyPoolExtension; } }
    }
}
